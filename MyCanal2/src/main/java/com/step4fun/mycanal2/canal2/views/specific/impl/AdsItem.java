package com.step4fun.mycanal2.canal2.views.specific.impl;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.views.specific.Item;
import com.step4fun.mycanal2.canal2.views.specific.RowType;

/**
 * Created by sngatchou on 20/03/2015.
 */
public class AdsItem extends Item {
    @Override
    public View getView(LayoutInflater inflater, int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.home_list_pub, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        initAds(holder);

        return convertView;
    }

    private void initAds(ViewHolder holder) {
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("0C30BD7C163126E88295E68F88CE257E")
                .build();
        holder.adSpace.loadAd(adRequest);
    }

    @Override
    public int getItemViewType() {
        return RowType.PUB.ordinal();
    }

    static class ViewHolder {
        @InjectView(R.id.adView)
        AdView adSpace;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
