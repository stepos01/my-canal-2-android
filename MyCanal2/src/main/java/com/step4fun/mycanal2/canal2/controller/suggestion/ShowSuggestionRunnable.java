package com.step4fun.mycanal2.canal2.controller.suggestion;

import com.step4fun.mycanal2.canal2.controller.events.ShowSuggestionQueryEvent;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;
import com.step4fun.mycanal2.canal2.views.fragment.ShowsFragment;
import de.greenrobot.event.EventBus;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephanekamga on 27/01/15.
 */
public class ShowSuggestionRunnable implements Runnable {

    private List<ProgramsModel> mModel;
    private String mQuery;

    public ShowSuggestionRunnable(List<ProgramsModel> model, String query)
   {
       mModel = model;mQuery = query;
   }

    @Override
    public void run() {
        List<ProgramsModel> result = new ArrayList<>();
        for (ProgramsModel model : mModel)
        {
            if (model.getType() == ShowsFragment.VIEW_TYPE_HEADER) {
                if (clean(model.getSection()).contains(clean(mQuery))) result.add(model);
            } else {
                //            //todo add time parsed with joda
                if (model.getTitle().toLowerCase().contains(mQuery.toLowerCase())) result.add(model);
                else if (model.getSection().toLowerCase().contains(mQuery.toLowerCase())) result.add(model);
                else if (model.getSynopsis().toLowerCase().contains(mQuery.toLowerCase())) result.add(model);
            }
        }
        EventBus.getDefault().post(new ShowSuggestionQueryEvent(mQuery, result));
    }
    public static String clean(String s)
    {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[^\\p{InCombiningDiacriticalMarks}]", "");
        return s.toLowerCase();
    }
}
