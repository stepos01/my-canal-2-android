package com.step4fun.mycanal2.canal2.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by stephanekamga on 02/02/15.
 */
public class ShowsVideo implements Parcelable, Cloneable {

    public String videoId;

    public boolean isPlaylist;

    public String imageUrl;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean isPlaylist() {
        return isPlaylist;
    }

    public void setPlaylist(boolean isPlaylist) {
        this.isPlaylist = isPlaylist;
    }

    public String getVideoId() {
        return videoId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public ShowsVideo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<ShowsVideo> CREATOR
            = new Creator<ShowsVideo>() {
        public ShowsVideo createFromParcel(Parcel in) {
            return new ShowsVideo(in);
        }

        public ShowsVideo[] newArray(int size) {
            return new ShowsVideo[size];
        }
    };

    public ShowsVideo(Parcel in) {
        videoId = in.readString();
        imageUrl = in.readString();
        isPlaylist = in.readInt()==1;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoId);
        dest.writeString(imageUrl);
        dest.writeInt(isPlaylist?1:0);

    }

//    public class Builder{
//
//        public Builder setId(String id)
//        {
//             setVideoId(id);return this;
//        }
//        public Builder IsPlaylist(boolean pIsPlaylist)
//        {
//            setPlaylist(pIsPlaylist);return this;
//        }
//        public  ShowsVideo Build(YoutubeClient client)
//        {
//           if(!isPlaylist)setImageUrl(client.getThumbForVideo(videoId));
//            else {
//               List<String> thumbs = client.getThumbForPlaylist(videoId);
//               Random r = new Random();
//               setImageUrl(thumbs.get(r.nextInt(thumbs.size())));
//           }
//            return ShowsVideo.this;
//
//        }
//    }
}
