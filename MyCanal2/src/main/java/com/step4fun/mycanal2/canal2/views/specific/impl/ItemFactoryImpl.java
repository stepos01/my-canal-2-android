package com.step4fun.mycanal2.canal2.views.specific.impl;

import com.step4fun.mycanal2.canal2.model.Comments;
import com.step4fun.mycanal2.canal2.views.specific.Item;
import com.step4fun.mycanal2.canal2.views.specific.ItemFactory;
import com.step4fun.mycanal2.canal2.views.specific.RowType;

import java.util.List;

/**
 * Created by sngatchou on 19/03/2015.
 */
public class ItemFactoryImpl<T extends Comments> implements ItemFactory<T> {
    @Override
    public Item createItem(T data, String itemType) {
        Item item = null;
        if (itemType.equals(RowType.HEADER.getType())) {
            item = new HeaderItem();
        } else if (itemType.equals(RowType.ROW.getType())) {
            item = new RowItem<T>(data);
        } else if (itemType.equals(RowType.NO_COMMENTS.getType())) {
            item = new NoCommentsItem();
        } else if (itemType.equals(RowType.PUB.getType())) {
            item = new AdsItem();
        } else if (itemType.equals(RowType.ERROR.getType())) {
            item = new ErrorItem();
        }

        return item;
    }
}
