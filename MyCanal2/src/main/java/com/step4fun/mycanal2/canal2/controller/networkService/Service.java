package com.step4fun.mycanal2.canal2.controller.networkservice;

import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.controller.events.AppInfoEvent;
import com.step4fun.mycanal2.canal2.controller.events.AppInfoEventException;
import com.step4fun.mycanal2.canal2.controller.events.PostNewCommentException;
import com.step4fun.mycanal2.canal2.controller.events.ProgramModelChangedEvent;
import com.step4fun.mycanal2.canal2.model.AppInfo;
import com.step4fun.mycanal2.canal2.model.Comments;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;
import com.step4fun.mycanal2.canal2.model.UserShowPreference;
import de.greenrobot.event.EventBus;
import io.vov.vitamio.utils.Log;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.HashMap;
import java.util.List;

/**
 * Created by stephanekamga on 16/03/15.
 */
public class Service {

    private Service() {
        restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://54.76.90.254:8080/mycanal2")
                .build();
    }

    private RestAdapter restAdapter;
    private static Service instance;

    public static Service getInstance() {
        if (instance == null) instance = new Service();
        return instance;
    }

    public void getShows(Callback<List<ProgramsModel>> cb) {
        restAdapter.create(ShowsApi.class).listShows(cb);
    }

    /**
     * @param model the programModel  param
     * @param
     */
    public void incrementViews(final ProgramsModel model) {
        restAdapter.create(ShowsApi.class).incrementViewCount(model.getId(), new Callback<ProgramsModel>() {
            @Override
            public void success(ProgramsModel result, Response response) {
                Log.d(getClass().getSimpleName(), "success inc views count for model with id:" + model.getId());
                EventBus.getDefault().post(new ProgramModelChangedEvent(result));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(getClass().getSimpleName(), "failure inc views count for model with id:" + model.getId());
                EventBus.getDefault().post(new ProgramModelChangedEvent(model));
            }
        });
    }

    public void incrementLikes(final ProgramsModel model) {
        restAdapter.create(ShowsApi.class).incrementLikeCount(model.getId(), 1, new Callback<ProgramsModel>() {
            @Override
            public void success(ProgramsModel result, Response response) {
                Log.d(getClass().getSimpleName(), "success inc likes count for model with id:" + model.getId());
                EventBus.getDefault().post(new ProgramModelChangedEvent(result));
            }

            @Override
            public void failure(RetrofitError error) {
                EventBus.getDefault().post(new ProgramModelChangedEvent(model));
                Log.d(getClass().getSimpleName(), "failure inc likes count for model with id:" + model.getId());
                UserShowPreference.getInstance(Utils.getActivity()).switchPreference(model);
            }
        });
    }

    public void decrementLikes(final ProgramsModel model) {
        restAdapter.create(ShowsApi.class).incrementLikeCount(model.getId(), -1, new Callback<ProgramsModel>() {
            @Override
            public void success(ProgramsModel result, Response response) {
                Log.d(getClass().getSimpleName(), "success dec likes count for model with id:" + model.getId());
                EventBus.getDefault().post(new ProgramModelChangedEvent(result));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(getClass().getSimpleName(), "failure dec likes count for model with id:" + model.getId());
                EventBus.getDefault().post(new ProgramModelChangedEvent(model));
            }
        });
    }

    public void updateShow(ProgramsModel model, Callback<ProgramsModel> cb) {
        restAdapter.create(ShowsApi.class).updateShow(model, cb);
    }

    public void getStreamUrl(final Callback<String> cb) {
        restAdapter.create(StreamApi.class).getStreamUrl(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String result = Utils.parseStream(response);
                if (Utils.isNullOrEmpty(result))
                    cb.failure(RetrofitError.unexpectedError("", new RuntimeException("retrived a null result from body when getting stream url")));
                else cb.success(result, response);
            }

            @Override
            public void failure(RetrofitError error) {
                cb.failure(error);
            }
        });
    }

    /**
     * @param cb
     * @param count the number of comments we want to retrieve
     * @param next  next should be the "Comments.datetime" value of the last element inside the list of comments
     *              previously received or 0 in case no comments was previously retrieved
     */
    public void getAllComments(Callback<List<Comments>> cb, long next, int count) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("next", String.valueOf(next));
        map.put("count", String.valueOf(count));
        restAdapter.create(CommentsApi.class).loadComments(map, cb);
    }

    public void getAllComments(Callback<List<Comments>> cb) {
        HashMap<String, String> map = new HashMap<String, String>();
        restAdapter.create(CommentsApi.class).loadComments(map, cb);
    }

    /**
     * @param comments
     * @param cb       the response is the one sent from the server
     */
    public void addnewComment(final Comments comments, final Callback<Boolean> cb) {
        restAdapter.create(CommentsApi.class).newComment(comments, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                Log.d(getClass().getSimpleName(), "success adding comment");
                if (response.getStatus() != 200)
                    cb.failure(RetrofitError.unexpectedError(response.getUrl(), new PostNewCommentException(comments)));
                else cb.success(true, response);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(getClass().getSimpleName(), "failure adding comment");
                cb.failure(RetrofitError.unexpectedError(error.getUrl(), new PostNewCommentException(comments)));
            }
        });
    }

    /**
     * Retrieve application information on the Play Store
     */
    public void getAppInfo() {
        restAdapter.create(AppInfoApi.class).loadAppInfos(new Callback<List<AppInfo>>() {
            @Override
            public void success(List<AppInfo> appInfos, Response response) {
                // Update the UI with download number
                EventBus.getDefault().post(new AppInfoEvent(appInfos.get(0)));
            }

            @Override
            public void failure(RetrofitError error) {
                // Update the UI with some wrong number
                EventBus.getDefault().post(new AppInfoEventException(error));
            }
        });
    }
}
