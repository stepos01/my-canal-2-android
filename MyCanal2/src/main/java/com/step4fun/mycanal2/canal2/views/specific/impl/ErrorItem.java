package com.step4fun.mycanal2.canal2.views.specific.impl;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.views.specific.Item;
import com.step4fun.mycanal2.canal2.views.specific.RowType;

/**
 * Created by sngatchou on 20/03/2015.
 */
public class ErrorItem extends Item {
    @Override
    public View getView(LayoutInflater inflater, int position, View convertView, ViewGroup parent) {
        return inflater.inflate(R.layout.home_list_error, parent, false);
    }

    @Override
    public int getItemViewType() {
        return RowType.ERROR.ordinal();
    }
}
