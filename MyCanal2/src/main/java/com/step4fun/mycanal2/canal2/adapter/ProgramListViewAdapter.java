package com.step4fun.mycanal2.canal2.adapter;//package com.step4fun.mycanal2.canal2.adapter;
//
//import android.content.Context;
//import android.net.Uri;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import com.bumptech.glide.Glide;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.mycanal2.R;
//import com.mycanal2.model.ProgramItemViewWrapper;
//import com.mycanal2.model.ProgramsModel;
//import com.step4fun.mycanal2.canal2.R;
//import com.step4fun.mycanal2.canal2.model.ProgramItemViewWrapper;
//import com.step4fun.mycanal2.canal2.model.ProgramsModel;
//
//import java.util.List;
//
///**
// * Created by stephanekamga on 17/01/15.
// */
//public class ProgramListViewAdapter extends ArrayAdapter<ProgramsModel> {
//
////    private FavoritesInfoProvider mFavoritesInfoProvider;
//    public static final int AdsEach = 6;
//    public ProgramListViewAdapter(Context context, int resource, List<ProgramsModel> objects) {
//        super(context, resource, objects);
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return 2;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//         if((position%AdsEach)==0)return 1;
//        else return 0;
//    }
//
//    @Override
//    public View getView(int position, View view, ViewGroup parent) {
//        final ProgramItemViewWrapper wrapper;
//        View convertView = view;
//
//        final ProgramsModel current = getItem(position);
//
//        if(convertView==null)
//        {
//            LayoutInflater inflate = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//            if(getItemViewType(position)==0)convertView = inflate.inflate(R.layout.programs_item,null);
//            else {
//                convertView = inflate.inflate(R.layout.adslayout,null);
//                assert getItem(position)==null;
//                AdView mAdView =(AdView) convertView.findViewById(R.id.adlayoutview);
//                AdRequest adRequest = new AdRequest.Builder().addTestDevice("0C30BD7C163126E88295E68F88CE257E")
//                    .build();
//            mAdView.loadAd(adRequest);
//                mAdView.resume();
//            }
//
//            wrapper = new ProgramItemViewWrapper(convertView);
//            convertView.setTag(wrapper);
//        }else {
//            wrapper =(ProgramItemViewWrapper) convertView.getTag();
//        }
//        if(getItemViewType(position)==0) {
//            String url = current.getImageUrl();
//            Glide.with(getContext()).load(Uri.parse(url)).crossFade().into(wrapper.getmImageView());
//            wrapper.getMsynopsisView().setText(parseSynopsisText(current.getSynopsis()));
//            wrapper.getMtimeView().setText(current.getFakeTime());
//            wrapper.getMtitleView().setText(current.getTitle());
////            mFavoritesInfoProvider = FavoriteInfoProviderImpl.getInstance((Activity) getContext());
////            wrapper.setIsFavorites(mFavoritesInfoProvider.isFavorites(current.getId()));
////            wrapper.getFavoritesView().setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////                    wrapper.setIsFavorites(!mFavoritesInfoProvider.isFavorites(current.getId()));
////                    mFavoritesInfoProvider.switchFavorites(current);
////                }
////            });
//        }else{
//
//        }
//
//        return convertView;
//    }
//
//
//
//    private String parseSynopsisText(String synopsis)
//    {
//        if(synopsis.length()>120)return synopsis.subSequence(0,117).toString()+"...";
//        else return synopsis;
//    }
//
//
//}
