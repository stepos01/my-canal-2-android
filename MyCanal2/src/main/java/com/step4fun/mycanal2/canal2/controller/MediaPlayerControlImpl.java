package com.step4fun.mycanal2.canal2.controller;

//import android.media.MediaPlayer;
import com.step4fun.mycanal2.canal2.views.VideoControllerView;
import io.vov.vitamio.MediaPlayer;

/**
 * Created by stephanekamga on 17/02/15.
 */
public class MediaPlayerControlImpl implements  VideoControllerView.MediaPlayerControl {
    private MediaPlayer player;
    public MediaPlayerControlImpl(MediaPlayer player) {
        this.player = player;
    }

    @Override
    public void start() {
        if(player!=null)
            player.start();
    }

    @Override
    public void pause() {
        if(player!=null)
            player.pause();
    }

    @Override
    public int getDuration() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return 0;
    }

    @Override
    public void seekTo(int pos) {

    }

    @Override
    public boolean isPlaying() {
        if(player!=null)return player.isPlaying();
        else return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return false;
    }

    @Override
    public boolean canSeekForward() {
        return false;
    }

    @Override
    public boolean isFullScreen() {
        return false;
    }

    @Override
    public void toggleFullScreen() {

    }
}
