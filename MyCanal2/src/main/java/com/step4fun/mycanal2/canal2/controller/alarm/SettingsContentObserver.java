package com.step4fun.mycanal2.canal2.controller.alarm;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.util.Log;

/**
 * Created by stephanekamga on 17/01/15.
 */
public class SettingsContentObserver extends ContentObserver {
    int previousVolume;
    VolumeChangedListener mVolumeChangedListener;
    Context context;

    public SettingsContentObserver(Context c, Handler handler) {
        super(handler);
        context=c;

        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
    }
    public void addListener(VolumeChangedListener listener){
         mVolumeChangedListener = listener;
    }
    @Override
    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);

        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);

        int delta=previousVolume-currentVolume;

        if(delta>0)
        {
            Log.d("Volume: ","Decreased");
            previousVolume=currentVolume;
            if(mVolumeChangedListener!=null)mVolumeChangedListener.VolumeChanged(previousVolume);
        }
        else if(delta<0)
        {
            Log.d("Volume: ","Increased");
            previousVolume=currentVolume;
            if(mVolumeChangedListener!=null)mVolumeChangedListener.VolumeChanged(previousVolume);
        }
    }
    public interface VolumeChangedListener
    {
        void VolumeChanged(int newVolume);
    }
}
