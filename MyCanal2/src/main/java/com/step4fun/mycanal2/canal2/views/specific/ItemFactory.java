package com.step4fun.mycanal2.canal2.views.specific;

import com.step4fun.mycanal2.canal2.model.Comments;

import java.util.List;

/**
 * Created by sngatchou on 19/03/2015.
 */
public interface ItemFactory<T extends Comments> {
    public Item createItem(T data, String itemType);
}

