package com.step4fun.mycanal2.canal2.views.fragment;//package com.step4fun.mycanal2.canal2.views.fragment;
//
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.DialogFragment;
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.PopupMenu;
//import android.widget.TextView;
//import com.bumptech.glide.Glide;
//import com.mycanal2.R;
//import com.mycanal2.controller.CalendarService;
//import com.mycanal2.controller.ReminderService;
//import com.mycanal2.model.ProgramsModel;
//import com.mycanal2.views.MainActivity;
//
///**
// * Created by stephanekamga on 18/01/15.
// */
//public class ProgramsDetailDialog extends DialogFragment implements Toolbar.OnMenuItemClickListener, PopupMenu.OnMenuItemClickListener, View.OnClickListener {
//    private static final String DIALOG_MODEL = "DIALOG_MODEL";
//    private PopupMenu popupMenu;
//    private Toolbar toolbar;
//    private TextView textView;
//    private ProgramsModel model;
//    private ReminderService mReminderService;
//    private boolean isReminderChanged = false;
//    private static ProgramsDetailDialog mInstance = new ProgramsDetailDialog();
//    public ProgramsDetailDialog() {
//        super();
//    }
//    public static ProgramsDetailDialog getInstance(ProgramsModel model)
//    {
//        Bundle b = new Bundle();
//        b.putParcelable(DIALOG_MODEL,model);
//        mInstance.setArguments(b);
//        return mInstance;
//
//    }
//    private ProgramsModel getModel()
//    {
//        if(model == null)model = (ProgramsModel) getArguments().get(DIALOG_MODEL);
//        return model;
//    }
//
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        AlertDialog.Builder builder = new AlertDialog.Builder(getThemeContext());
//
//        ProgramsModel model = getModel();
//        View v = inflater.inflate(R.layout.programs_details,null);
//        ((TextView)v.findViewById(R.id.programs_details_title)).setText(model.getTitle());
//        ((TextView)v.findViewById(R.id.programs_details_description)).setText(model.getSynopsis());
//        toolbar = (Toolbar)v.findViewById(R.id.mytoolbar);
//        ImageView view = (ImageView)v.findViewById(R.id.programs_details_image);
//        Glide.with(getActivity()).load(Uri.parse(model.getImageUrl())).crossFade().into(view);
//        toolbar.setOnMenuItemClickListener(this);
//        textView =(TextView)toolbar.findViewById(R.id.reminder_button);
//
//        if(mReminderService!=null&&mReminderService.hasReminder(model))
//        {
//            textView.setTextColor(getResources().getColor(R.color.Green));
//            textView.setCompoundDrawables(getResources().getDrawable(R.drawable.appointmentreminders_selected),null,null,null);
//        }
//         textView.setOnClickListener(this);
////        Spinner spinner = (Spinner)toolbar.findViewById(R.id.spinner);
////        spinnerAdapter = ArrayAdapter.createFromResource(getActivity(),R.array.set_alarm_array,android.R.layout.simple_spinner_item);
////        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
////        spinner.setAdapter(spinnerAdapter);
//
//        toolbar.inflateMenu(R.menu.programdetailsmenu);
//
//        builder.setView(v);
//        return builder.create();
//
//    }
//
//    public Context getThemeContext()
//    {
//        return ((MainActivity)getActivity()).getSupportActionBar().getThemedContext();
//    }
//
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//     //   this.debug(true);
////        this.setBlurRadius(4);
////        this.setDownScaleFactor(5.0f);
//        if(ReminderService.canUseCalendarICS())mReminderService = new CalendarService((MainActivity)getActivity());
//        else{
//            //todo set up an alarm and notification service
//        }
//    }
//
//
//    @Override
//    public boolean onMenuItemClick(MenuItem menuItem) {
//        switch (menuItem.getItemId())
//        {
//
//            case R.id.action_share:
//            {
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                String shareText = "I'm watching "+getModel().getTitle()+" using "+getResources().getString(R.string.app_name)+" App for Android";
//                sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
//                sendIntent.setType("text/plain");
//                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
//                break;
//            }
////            case R.id.action_set_alarm:
////            {
////
////                popupMenu = new PopupMenu(getThemeContext(),toolbar.findViewById(R.id.action_set_alarm));
////                MenuInflater inflater = popupMenu.getMenuInflater();
////                inflater.inflate(R.menu.program_details_alarm_menu,popupMenu.getMenu());
////                popupMenu.setOnMenuItemClickListener(ProgramsDetailDialog.this);
////
////                popupMenu.show();
////            }
////            case R.id.action_set_alarm_5min_before:
////            {
//////                textView.setText(R.string.ten_min_before);
//////                getModel().setReminder(ProgramsModel.ReminderType.TenMinutes);
//////                FavoriteInfoProviderImpl.getInstance(getActivity()).addFavorite(getModel());
//////                break;
//////            }
//////            case R.id.action_set_alarm_one_hour_before:
//////            {
//////                textView.setText(R.string.one_hour_before);
//////                getModel().setReminder(ProgramsModel.ReminderType.OneHour);
//////                FavoriteInfoProviderImpl.getInstance(getActivity()).addFavorite(getModel());
//////                break;
//////
//////            }
//        }
//        return true;
//    }
//
//    @Override
//    public void onDismiss(DialogInterface dialog) {
//
//        if(isReminderChanged)
//        {
////            if(mReminderService.hasReminder(model.getId()))
////            {
////                //we remove the reminder
////                mReminderService.removeReminder(model);
////            }else{
////                //we add the reminder
////                mReminderService.addReminder(model);
////            }
//        }
//        super.onDismiss(dialog);
//    }
//
//    @Override
//    public void onClick(View v) {
//
////            popupMenu = new PopupMenu(getThemeContext(),v);
////            MenuInflater inflater = popupMenu.getMenuInflater();
////            inflater.inflate(R.menu.program_details_alarm_menu,popupMenu.getMenu());
////            popupMenu.setOnMenuItemClickListener(ProgramsDetailDialog.this);
////
////            popupMenu.show();
//        if(mReminderService==null)return;
//        isReminderChanged = !isReminderChanged;
//        if(mReminderService.hasReminder(getModel().getId()))
//        {
//            textView.setTextColor(getResources().getColor(R.color.White_GhostWhite));
//            textView.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.appointmentreminders), null, null, null);
//
//        }else {
//            textView.setTextColor(getResources().getColor(R.color.Green));
//            textView.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.appointmentreminders_selected), null, null, null);
//        }
//    }
//}
