package com.step4fun.mycanal2.canal2.views.specific.impl;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.views.specific.Item;
import com.step4fun.mycanal2.canal2.views.specific.RowType;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by sngatchou on 19/03/2015.
 */
public class HeaderItem extends Item {
    public HeaderItem() {
    }

    @Override
    public View getView(LayoutInflater inflater, int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.home_list_header, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        displayView(holder);

        return convertView;
    }

    private void displayView(ViewHolder holder) {
        String userAccount = Utils.getPrimaryAccount();
        if (userAccount != null && userAccount.length() > 0)
            holder.headerUsername.setText(Utils.buildUsername(Utils.split(userAccount)));
    }

    @Override
    public int getItemViewType() {
        return RowType.HEADER.ordinal();
    }

    static class ViewHolder {
        @InjectView(R.id.home_list_header_profile_img_id)
        CircleImageView profileImage;
        @InjectView(R.id.home_list_header_username)
        TextView headerUsername;
        @InjectView(R.id.home_list_header_app_users_count_id)
        TextView subscriber;
        @InjectView(R.id.home_list_header_write)
        ImageButton pencil;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
