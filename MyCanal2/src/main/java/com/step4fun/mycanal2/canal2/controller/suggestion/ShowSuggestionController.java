package com.step4fun.mycanal2.canal2.controller.suggestion;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.widget.SearchView;
import com.step4fun.mycanal2.canal2.adapter.ShowListViewAdapter;
import com.step4fun.mycanal2.canal2.controller.events.ShowSuggestionQueryEvent;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;
import de.greenrobot.event.EventBus;

import java.util.ArrayList;

/**
 * Created by stephanekamga on 27/01/15.
 */
public class ShowSuggestionController implements SearchView.OnSuggestionListener, SearchView.OnCloseListener, SearchView.OnQueryTextListener {
    private Activity mContext;
    private SearchView mSearchView;
    private ArrayList<ProgramsModel> mItems;
    //    private long currentSearchingWorkerId;
    private  String TAG = getClass().getSimpleName();
    private boolean FASTSEARCHLOOKUP = true;
    private int searchThreshold = 3;
    private ShowListViewAdapter mAdapter;

    private String currentQuery;


    private static ShowSuggestionController instance;

    public static ShowSuggestionController getInstance(Activity context,SearchView searchView)
    {
        if (instance == null)instance = new ShowSuggestionController(context,searchView);
        if(!EventBus.getDefault().isRegistered(instance))EventBus.getDefault().register(instance);
        return instance;
    }
    private   ShowSuggestionController(Activity context,SearchView searchView)
    {
        mContext = context;mSearchView = searchView;
    }

    public void setItems(ArrayList<ProgramsModel> mItems) {
        this.mItems = mItems;
    }

    public void setAdapter(ShowListViewAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }

    public void Build(ArrayList mItems,ShowListViewAdapter adapter)
    {

        setItems(mItems);
        setAdapter(adapter);
        SearchManager mSearchManager = (SearchManager) mContext.getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(mContext.getComponentName()));
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnSuggestionListener(this);
        mSearchView.setOnCloseListener(this);
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return true;
    }

    @Override
    public boolean onSuggestionClick(int position) {
//        mAdapter.clear();
//        ArrayList<ProgramsModel>result = new ArrayList<>();
//        if(mSuggestedList.get(position).getType()== ShowsFragment.VIEW_TYPE_HEADER)
//        {
//            //it's a section
//            result = withSection(mSuggestedList.get(position).getSection());
//        }else{
//            //it's a show
//            result.add(getSection(mSuggestedList.get(position).getSection()));
//            result.add(mSuggestedList.get(position));
//        }
//        mAdapter.addAll(result);
//
//        mAdapter.notifyDataSetChanged();
        return true;
    }


    @Override
    public boolean onClose() {
        mAdapter.setItem(mItems);
        EventBus.getDefault().unregister(this);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(final String query) {

        if(query.length()==0){
            mAdapter.setItem(mItems);

            return true;
        }
        if(FASTSEARCHLOOKUP)
        {
            searchThreshold = 1;
        }


        if (query.length() > searchThreshold) {
            ShowSuggestionService.getInstance(mItems).Search(query);
            currentQuery = query;
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    setActiveWorker(Thread.currentThread().getId());
//                    try {
//                        mSuggestedList = ProcessNLP(query);
//                        new Handler(Looper.getMainLooper()).post(new Runnable() {
//                            @Override
//                            public void run() {
//                                loadData(mSuggestedList);
//                            }
//                        });
//                    } catch (InterruptedException | ExecutionException e) {
//                        Log.d(TAG, e.getMessage());
//                    }
//
//                }
//            }).start();
        }

        return true;
    }

    public void onEventMainThread(ShowSuggestionQueryEvent event) {
        if (currentQuery.equals(event.getQuery()))
        {
            mAdapter.setItem(new ArrayList<ProgramsModel>(event.getShows()));

        }

    }
    //we fist deal with the section title
    //we add all shows indicated to by a section
    //then we add all the rest
    //finally we ordered the whole list based on the header section
//    private void withoutSuggestionSearch(final List<ProgramsModel> suggested)
//    {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                HashMap<String,List<ProgramsModel>>map = new HashMap<>();
//                for(ProgramsModel model : suggested)
//                {
//                    if(model.getType()==ShowsFragment.VIEW_TYPE_HEADER)
//                    {
//                        for (ProgramsModel programsModel : withSectionWithoutHeader(model.getSection())) {
//                            insert(map,programsModel);
//                        }
//
//                    } else{
//                        insert(map,model);
//                    }
//                }
//                final ArrayList<ProgramsModel> result = new ArrayList<>();
//                for(String key: map.keySet())
//                {
//                    result.add(new ProgramsModel(key,ShowsFragment.VIEW_TYPE_HEADER));
//                    for(ProgramsModel m : map.get(key))
//                    {
//                        result.add(m);
//                    }
//                }
//                new Handler(Looper.getMainLooper()).post(new Runnable() {
//                    @Override
//                    public void run() {
//                        mAdapter.clear();
//
//                        mAdapter.addAll(result);
//                        mAdapter.notifyDataSetChanged();
//                    }
//                });
//            }
//        }).start();
//
//
//
//    }

//    private void insert(HashMap<String,List<ProgramsModel>> map,ProgramsModel value) {
//
//        if (!map.keySet().contains(value.getSection()))map.put(value.getSection(),Arrays.asList(value));
//        else{
//            List<ProgramsModel> list = new ArrayList<>(map.get(value.getSection()));
//            list.add(value);
//            map.put(value.getSection(), list);
//        }
//    }

//    private synchronized boolean isActiveWorker(long threadId)
//    {
//        synchronized (lock)
//        {
//            return threadId == currentSearchingWorkerId;
//        }
//    }
//    private synchronized void setActiveWorker(long threadId)
//    {
//        synchronized (lock)
//        {
//            currentSearchingWorkerId = threadId;
//
//        }
//    }
//    private ArrayList<ProgramsModel> ProcessNLP(String query) throws InterruptedException, ExecutionException {
//        FutureTask<ArrayList<ProgramsModel>> futureTask = new FutureTask<ArrayList<ProgramsModel>>(ShowSuggestionService.getInstance(mItems).setQuery(query));
//        futureTask.run();
//        final Object o = new Object();
//        synchronized (o)
//        {
//            while (!futureTask.isDone()&&!futureTask.isCancelled()&&isActiveWorker(Thread.currentThread().getId()))
//            {
//                o.wait(100);
//            }
//        }
//        if(isActiveWorker(Thread.currentThread().getId()))
//        {
//            if(!futureTask.isDone())return new ArrayList<>();
//            else return futureTask.get();
//        }else{
//            Thread.currentThread().interrupt();
//        }
//
//        return null;
//    }
//    private void loadData(List<ProgramsModel> list) {
//        // Cursor
//
//        if (list == null || list.size() == 0) return;
//
//        if(FASTSEARCHLOOKUP)
//        {
//            withoutSuggestionSearch(list);
//
//        }else {
//
//
//            String[] columns = new String[]{"_id", "text"};
//            Object[] temp = new Object[]{0, "default"};
//
//            MatrixCursor cursor = new MatrixCursor(columns);
//
//            for (int i = 0; i < list.size(); i++) {
//
//                temp[0] = i;
//                if (list.get(i).getType() == ShowsFragment.VIEW_TYPE_HEADER) temp[1] = list.get(i).getSection();
//                else temp[1] = list.get(i).getTitle();
//                cursor.addRow(temp);
//
//            }
//            //   mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
//
//
//            // SearchView
//
//            if (mSearchView.getSuggestionsAdapter() == null)
//                mSearchView.setSuggestionsAdapter(new ShowFragmentSearchViewAdapter(mContext, cursor, list));
//            else {
//                mSearchView.getSuggestionsAdapter().changeCursor(cursor);
//                mSearchView.getSuggestionsAdapter().notifyDataSetChanged();
//            }
//        }
//
//    }
//    private ArrayList<ProgramsModel> withSection(String section)
//    {
//        ArrayList<ProgramsModel> result = new ArrayList<>();
//        result.add(getSection(section));
//        for(ProgramsModel model : mItems)
//        {
//            if(model.getType()==ShowsFragment.VIEW_TYPE_SHOW&&section.equals(model.getSection()))result.add(model);
//        }
//        return result;
//    }
//    private ArrayList<ProgramsModel> withSectionWithoutHeader(String section)
//    {
//        ArrayList<ProgramsModel> result = new ArrayList<>();
//        for(ProgramsModel model : mItems)
//        {
//            if(model.getType()==ShowsFragment.VIEW_TYPE_SHOW&&section.equals(model.getSection()))result.add(model);
//        }
//        return result;
//    }
//    private ProgramsModel getSection(String section)
//    {
//        for(ProgramsModel model: mItems)
//        {
//            if(section.equals(model.getSection()))return model;
//        }
//        return null;
//    }
}
