package com.step4fun.mycanal2.canal2.model;

import android.app.Activity;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by stephanekamga on 16/03/15.
 */
public class UserShowPreference extends DataSavedWrapper<ArrayList<ProgramsModel>> {

    private static final String preferredShows = "preferredShows";
    private UserShowPreference(Activity activity) {
        super(activity, preferredShows);
    }
    private static UserShowPreference instance;
    public static UserShowPreference getInstance(Activity activity)
    {
        if(instance==null)instance = new UserShowPreference(activity);
        return instance;
    }


    public ArrayList<ProgramsModel> loadPreferredShows()
    {
        TypeToken<ArrayList<ProgramsModel>> typeToken = new TypeToken<ArrayList<ProgramsModel>>() {
        };
        ArrayList<ProgramsModel> result = load(typeToken);
        if (result != null) return result;
        else
            return new ArrayList<ProgramsModel>();
    }

    public void savePreferedShows(ArrayList<ProgramsModel> models)
    {

        TypeToken<ArrayList<ProgramsModel>> typeToken = new TypeToken<ArrayList<ProgramsModel>>() {
        };
        save(models, typeToken);
    }
    public boolean isPreferredShow(ProgramsModel model)
    {
        List<ProgramsModel>result = loadPreferredShows();

        for (ProgramsModel target:result)
        {
            if(target.getId()==model.getId())return true;
        }
        return false;
    }
    public void addPreferredShow(ProgramsModel model)
    {
        ArrayList<ProgramsModel> result = loadPreferredShows();
        result.add(model);
        savePreferedShows(result);
    }
    public void removePreferredShow(ProgramsModel model)
    {
        ArrayList<ProgramsModel> result = loadPreferredShows();
        Iterator<ProgramsModel> it = result.iterator();
        while (it.hasNext())
        {
            if(it.next().getId()==model.getId())it.remove();
        }
        savePreferedShows(result);

    }
    public void switchPreference(ProgramsModel model)
    {
        if (isPreferredShow(model)) {
            removePreferredShow(model);
        } else {
            addPreferredShow(model);

        }
    }



}
