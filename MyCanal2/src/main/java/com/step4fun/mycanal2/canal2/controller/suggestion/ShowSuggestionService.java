package com.step4fun.mycanal2.canal2.controller.suggestion;

import com.step4fun.mycanal2.canal2.model.ProgramsModel;

import java.util.ArrayList;

/**
 * Created by stephanekamga on 27/01/15.
 */
public class ShowSuggestionService {

    private ArrayList<ProgramsModel> models;
    private static ShowSuggestionService Instance;

    public static ShowSuggestionService getInstance(ArrayList<ProgramsModel>model)
    {
        if(Instance==null)Instance = new ShowSuggestionService(model);
        return Instance;
    }

    private ShowSuggestionService( final ArrayList<ProgramsModel>models)
    {
        this.models = models;
//        mPool =   Executors.newFixedThreadPool(5);
    }

    public void Search(String query) {
        new Thread(new ShowSuggestionRunnable(models, query)).start();

    }


//    @Override
//    public ArrayList<ProgramsModel> call() throws Exception {
//        if (Utils.isNullOrEmpty(mQuery))return new ArrayList<>();
//        ArrayList<Callable<ProgramsModel>> callables = new ArrayList<>();
//
//        for(int i =0;i<models.size();i++)
//        {
//             callables.add(new ShowSuggestionRunnable(models.get(i),mQuery));
//        }
//        List<Future<ProgramsModel>> futures = mPool.invokeAll(callables);
//        shutdownAndAwaitTermination(mPool);
//        try {
//            for(int i =0;i< futures.size();i++) {
//               if(futures.get(i).isDone()&& futures.get(i).get()!=null) {
//                   result.add(futures.get(i).get()
//                   );
//                }
//            }
//        }catch (Exception e) {
//            Log.d(getClass().getSimpleName(),"error extracting result from futures");
//
//        }
//        return result;
//
//
//    }
//    void shutdownAndAwaitTermination(ExecutorService pool) {
//        pool.shutdown(); // Disable new tasks from being submitted
//        try {
//            // Wait a while for existing tasks to terminate
//            if (!pool.awaitTermination(2, TimeUnit.SECONDS))
//                pool.shutdownNow(); // Cancel currently executing tasks
//                // Wait a while for tasks to respond to being cancelled
////                if (!pool.awaitTermination(10, TimeUnit.SECONDS))
////                    System.err.println("Pool did not terminate");
//
//            } catch (InterruptedException ie) {
//                // (Re-)Cancel if current thread also interrupted
//                pool.shutdownNow();
//                // Preserve interrupt status
//                //Thread.currentThread().interrupt();
//            }
//        }
    }



