package com.step4fun.mycanal2.canal2.controller.events;

import com.step4fun.mycanal2.canal2.model.ProgramsModel;

/**
 * Created by stephanekamga on 20/03/15.
 */
public class ProgramModelChangedEvent implements EventStub {
    private ProgramsModel update;
    private ProgramsModel old;

    public ProgramModelChangedEvent(ProgramsModel update) {
        this.update = update;
    }

    public ProgramsModel getOld() {
        return old;
    }

    public void setOld(ProgramsModel old) {
        this.old = old;
    }

    public ProgramsModel getUpdate() {
        return update;
    }

    public void setUpdate(ProgramsModel update) {
        this.update = update;
    }
}
