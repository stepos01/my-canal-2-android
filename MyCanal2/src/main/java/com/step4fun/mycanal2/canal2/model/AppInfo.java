package com.step4fun.mycanal2.canal2.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sngatchou on 31/03/2015.
 */
public class AppInfo implements Parcelable {
    private String downloads;

    public AppInfo(Parcel source) {
        downloads = source.readString();
    }

    public static final Creator<AppInfo> CREATOR
            = new Creator<AppInfo>() {
        public AppInfo createFromParcel(Parcel in) {
            return new AppInfo(in);
        }

        public AppInfo[] newArray(int size) {
            return new AppInfo[size];
        }
    };

    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the set of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(downloads);
    }

    public String getDownloads() {
        return downloads;
    }
}
