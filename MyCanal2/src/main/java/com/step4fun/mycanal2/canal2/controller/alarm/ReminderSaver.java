package com.step4fun.mycanal2.canal2.controller.alarm;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.step4fun.mycanal2.canal2.MainActivity;
import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephanekamga on 22/01/15.
 */
class ReminderSaver implements IReminderSaver {
    private MainActivity mContext;
    private static final String REMINDER_PROGRAM_LIST = "REMINDER_PROGRAM_LIST";
    private static ReminderSaver Instance;

    private ReminderSaver(MainActivity context) {
        mContext = context;
    }

    public static ReminderSaver getInstance(MainActivity context) {
        if (Instance == null) Instance = new ReminderSaver(context);
        return Instance;
    }

    @Override
    public void setReminder(ProgramsModel model) {
        if (!hasReminder(model.getId())) {

            List<ProgramsModel> list = getReminderList();
            list.add(model);
            save(list);
        }
    }

    private void save(List<ProgramsModel> list) {
        Gson parser = new Gson();

        Type listType = new TypeToken<List<ProgramsModel>>() {
        }.getType();
        String json = parser.toJson(list, listType);
        Utils.SaveSettings(mContext, REMINDER_PROGRAM_LIST, json);
    }

    @Override
    public List<ProgramsModel> getReminderList() {
        String json = Utils.LoadSettings(mContext, REMINDER_PROGRAM_LIST);
        List<ProgramsModel> list = null;
        if (!Utils.isNullOrEmpty(json)) {
            Gson parser = new Gson();
            Type listType = new TypeToken<List<ProgramsModel>>() {
            }.getType();
            list = parser.fromJson(json, listType);
        } else list = new ArrayList<ProgramsModel>();
        return list;
    }

    @Override
    public void removeReminder(ProgramsModel model) {
        if (hasReminder(model.getId())) {
            List<ProgramsModel> list = getReminderList();
            list.remove(model);
            save(list);
        }
    }


    @Override
    public ProgramsModel getRemindedProgram(long id) {
        List<ProgramsModel> list = getReminderList();
        ProgramsModel result = null;
        for (ProgramsModel model : list) {
            if (model.getId() == (id)) result = model;
        }
        return result;
    }

    @Override
    public boolean hasReminder(long id) {
        List<ProgramsModel> list = getReminderList();
        for (ProgramsModel model : list) {
            if (model.getId() == (id)) return true;
        }
        return false;
    }

    @Override
    public void switchReminder(ProgramsModel model) {
        if (hasReminder(model.getId()))
            removeReminder(model);
        else setReminder(model);
    }
}
