package com.step4fun.mycanal2.canal2.adapter;

import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.AbsListView;
import com.step4fun.mycanal2.canal2.controller.twitterbootstrap.TweetAsyncLoader;
import com.step4fun.mycanal2.canal2.views.fragment.HomeFragment;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.TweetViewAdapter;

/**
 * Created by stephanekamga on 23/03/15.
 */
public class EndlessScrollListener implements AbsListView.OnScrollListener {
    private int visibleThreshold = 3;
    private int currentPage = 0;
    private int previousTotal = 0;
    private boolean loading = true;
    private TweetViewAdapter<CompactTweetView> adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    public EndlessScrollListener(TweetViewAdapter<CompactTweetView> pAdapter, SwipeRefreshLayout mSwipeRefreshLayout) {
         adapter = pAdapter;
        this.swipeRefreshLayout = mSwipeRefreshLayout;
    }


    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if(adapter.getCount()==0)return;
        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
                currentPage++;
            }
        }
        if (!loading & (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            // I load the next page of gigs using a background task,
            // but you can call any function here.
            Long[]arr = new Long[2];
            arr[0]= (long) HomeFragment.MAX_TWEET_ALLOWED;
            arr[1]= getTwitterMaxId();

           new TweetAsyncLoader().execute(arr);
            swipeRefreshLayout.setRefreshing(true);
            loading = true;
        }
    }
    public long getTwitterMaxId()
    {
        long max_id = Long.MAX_VALUE;
        for(Tweet t : adapter.getTweets())
       {
           if(t.id<max_id)max_id = t.id;
       }
        return max_id;
    }

}
