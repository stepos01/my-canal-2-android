package com.step4fun.mycanal2.canal2.controller.alarm;

import com.step4fun.mycanal2.canal2.model.ProgramsModel;

import java.util.List;

/**
 * Created by stephanekamga on 22/01/15.
 */
 interface IReminderSaver {

    public void setReminder(ProgramsModel model);
    public List<ProgramsModel> getReminderList();
    public void removeReminder(ProgramsModel model);
    public ProgramsModel getRemindedProgram(long id);
    public boolean hasReminder(long id);
    public void switchReminder(ProgramsModel model);
}
