package com.step4fun.mycanal2.canal2.views.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.step4fun.mycanal2.canal2.R;

/**
 * Created by stephanekamga on 22/01/15.
 */
public class DisclaimerFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.disclaimer,container,false);
    }
}
