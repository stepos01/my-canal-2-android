package com.step4fun.mycanal2.canal2;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import butterknife.ButterKnife;
import butterknife.InjectView;
import cn.pedant.SweetAlert.SweetAlertDialog;

import com.step4fun.mycanal2.canal2.controller.networkservice.Service;
import com.step4fun.mycanal2.canal2.controller.twitterbootstrap.TwitterAction;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;
import com.step4fun.mycanal2.canal2.views.fragment.*;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends BaseActivity {
    public static final String LiveFragmentTag = "LiveFragmentTag";
    public static final String ShowsFragmentTag = "ShowsFragmentTag";
    public static final String ProgFragmentTag = "ProgFragmentTag";
    public static final String AboutFragmentTag = "AboutFragmentTag";
    public static final String ShowDetailFragmentTag = "ShowDetailFragmentTag";
    public static final String DisclFragmentTag = "DisclFragmentTag";
    public static final String CURRENT_FRAGMENT_KEY = "CURRENT_FRAGMENT_KEY";
    private SweetAlertDialog dialog;
    private String mCurrentFragmentTag;
    @InjectView(R.id.mytoolbar)
    Toolbar toolbar;
    Callback<String> cb = new Callback<String>() {
        @Override
        public void success(String s, Response response) {
            if (!isCancelled) {
                if (!Utils.isNullOrEmpty(s)) {
                    dialog.dismiss();
                    LaunchPrincipal(s);
                } else onError();
            } else isCancelled = false;
        }

        @Override
        public void failure(RetrofitError error) {
            onError();
        }
    };

    private boolean isCancelled = false;

    public void initDialog() {
        dialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                .setContentText(getString(R.string.loading)).setTitleText("");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                isCancelled = true;
            }
        });
        dialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Fabric.with(this, new Crashlytics());
        if (!io.vov.vitamio.LibsChecker.checkVitamioLibs(this))
            return;
        // your code
        setContentView(R.layout.main);
        ButterKnife.inject(this);
        toolbar.inflateMenu(R.menu.main_menu);
        setSupportActionBar(toolbar);
        if (savedInstanceState != null) {
            //Restore the fragment's instance
            //mCurrentFragmentTag = savedInstanceState.getbun(CURRENT_FRAGMENT_KEY);
            Fragment f  = getSupportFragmentManager().getFragment(
                    savedInstanceState,"retain");
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentContainer,
                    f, getRetainFragmentTag(f));
            mCurrentFragmentTag = getRetainFragmentTag(f);
//           ft.commit();
            return;
        }
        Service.getInstance().getStreamUrl(cb);
        initDialog();
    }
    public String getRetainFragmentTag(Fragment f)
    {
         if(f instanceof HomeFragment)return LiveFragmentTag;
        else if(f instanceof ShowDetailsFragmentContainer)return ShowDetailFragmentTag;
        else if(f instanceof ShowsFragment)return ShowsFragmentTag;
        else if(f instanceof AboutFragment)return AboutFragmentTag;
        else throw new RuntimeException("");
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Save the fragment's instance
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);

        if(f!=null)getSupportFragmentManager().putFragment(outState, "retain", f);

    }

    public void LaunchPrincipal(String streamUrl) {
        if (!LiveFragmentTag.equals(mCurrentFragmentTag)) {
            mCurrentFragmentTag = LiveFragmentTag;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment f = getFragment(LiveFragmentTag, null);
            Bundle b = new Bundle();
            b.putString(HomeFragment.STREAM_URL, streamUrl);
            f.setArguments(b);
            ft.replace(R.id.fragmentContainer,
                    f, LiveFragmentTag);
            ft.commit();
        }
    }

    public Fragment getFragment(String tag, Bundle b) {
        Fragment f = getSupportFragmentManager().findFragmentByTag(tag);
        if (f == null) f = createFragment(tag, b);
        return f;
    }

    public Fragment createFragment(String tag, Bundle b) {
        if (tag.equals(LiveFragmentTag)) return new HomeFragment();
            //  else if( tag.equals(ProgFragmentTag)) return new ProgramsContainerFragment();
        else if (tag.equals(AboutFragmentTag)) return new AboutFragment();
        else if (tag.equals(DisclFragmentTag)) return new DisclaimerFragment();
        else if (tag.equals(ShowsFragmentTag)) return new ShowsFragment();
        else if (tag.equals(ShowDetailFragmentTag)) return ShowDetailsFragmentContainer.getInstance(b);
        else return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                // Remove the Up navigation button
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                return true;

            case R.id.action_live:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                LaunchPrincipal(null);
                return true;

//            case R.id.action_programs:
//                //LaunchPrograms();
//                return true;

            case R.id.action_shows:
                LaunchShows();
                return true;

            case R.id.action_about:
                LaunchAbout();
                return true;

            case R.id.action_disclaimer:
                LaunchDisclaimer();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void LaunchShows() {
        if (!ShowsFragmentTag.equals(mCurrentFragmentTag)) {
            mCurrentFragmentTag = ShowsFragmentTag;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment f = getFragment(ShowsFragmentTag, null);
            ft.replace(R.id.fragmentContainer,
                    f, ShowsFragmentTag);
            ft.commit();
        }
    }

    public void LaunchProgramDetails(ProgramsModel model) {
//        if (!ShowDetailFragmentTag.equals(mCurrentFragmentTag)) {
        mCurrentFragmentTag = ShowDetailFragmentTag;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Bundle b = new Bundle();
        b.putParcelable(ShowDetailsFragmentContainer.PROGRAM_MODEL_PARCEL_KEY, model);
        Fragment f = getFragment(ShowDetailFragmentTag, b);
        ft.replace(R.id.fragmentContainer,
                f, ShowDetailFragmentTag).addToBackStack(null);
        ft.commit();
    }


    public void LaunchDisclaimer() {
        if (!DisclFragmentTag.equals(mCurrentFragmentTag)) {
            mCurrentFragmentTag = DisclFragmentTag;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment f = getFragment(DisclFragmentTag, null);
            ft.replace(R.id.fragmentContainer,
                    f, DisclFragmentTag);
            ft.commit();
        }
    }

    public void LaunchAbout() {
        if (!AboutFragmentTag.equals(mCurrentFragmentTag)) {
            mCurrentFragmentTag = AboutFragmentTag;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment f = getFragment(AboutFragmentTag, null);
            ft.replace(R.id.fragmentContainer,
                    f, AboutFragmentTag);
            ft.commit();
        }
    }

    public void LaunchPrograms() {
        if (!ProgFragmentTag.equals(mCurrentFragmentTag)) {
            mCurrentFragmentTag = ProgFragmentTag;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment f = getFragment(ProgFragmentTag, null);
            ft.replace(R.id.fragmentContainer,
                    f, ProgFragmentTag);
            ft.commit();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        TwitterAction.setAuthResult(requestCode, responseCode, intent);
    }

    public boolean onError() {
        dialog.setContentText(getResources().getString(R.string.oops))
                .showCancelButton(true)
//                .setCancelText(getResources().getString(R.string.error_dialog_cancel))
                .setConfirmText(getResources().getString(R.string.error_dialog_retry))
//                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sweetAlertDialog) {
//                        sweetAlertDialog.dismiss();
//                        mProvider.setOnLoadedListener(null);
//                        mProvider.load();
//                    }
//                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.setContentText(getResources().getString(R.string.loading));
                        Service.getInstance().getStreamUrl(cb);
                        sweetAlertDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                    }
                });
        dialog.setCanceledOnTouchOutside(false);
        dialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);

        return true;
    }


}
