package com.step4fun.mycanal2.canal2.controller.events;

import com.step4fun.mycanal2.canal2.model.ProgramsModel;

import java.util.List;

/**
 * Created by stephanekamga on 20/03/15.
 */
public class ShowSuggestionQueryEvent
        implements EventStub {

    private String query;
    private List<ProgramsModel> shows;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public ShowSuggestionQueryEvent(String query, List<ProgramsModel> shows) {
        this.shows = shows;
        this.query = query;
    }

    public List<ProgramsModel> getShows() {
        return shows;
    }
}
