package com.step4fun.mycanal2.canal2.controller.events;

import retrofit.RetrofitError;

/**
 * Created by sngatchou on 31/03/2015.
 */
public class AppInfoEventException implements EventStub {
    private RetrofitError cause;

    public AppInfoEventException(RetrofitError error) {
        this.cause = error;
    }

    public RetrofitError getCause() {
        return cause;
    }
}
