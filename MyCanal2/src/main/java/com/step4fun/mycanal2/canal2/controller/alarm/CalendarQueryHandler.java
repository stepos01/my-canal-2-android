package com.step4fun.mycanal2.canal2.controller.alarm;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by stephanekamga on 22/01/15.
 */
public class CalendarQueryHandler extends AsyncQueryHandler{

    private OnCalendarQueryCompletedListener mListener;
    public static final int LOAD_CALENDAR_TOKEN_ID = 1000;
    public static final int CREATE_EVENT_TOKEN_ID = 1001;
    public static final int ADD_REMINDER = 1002;
    public static final int REMOVE_EVENT_TOKEN = 1003;
    public static final int CREATE_CALENDAR_ID = 1004;



    public CalendarQueryHandler(ContentResolver cr) {
        super(cr);
    }

    public void setListener(OnCalendarQueryCompletedListener mListener) {
        this.mListener = mListener;
    }

    @Override
    protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
        super.onQueryComplete(token, cookie, cursor);
         if(mListener!=null&&token==LOAD_CALENDAR_TOKEN_ID)mListener.OnLoadCalendarId(token,cookie,cursor);

    }

    @Override
    protected void onInsertComplete(int token, Object cookie, Uri uri) {
        super.onInsertComplete(token, cookie, uri);
        if(mListener!=null&&token==CREATE_EVENT_TOKEN_ID)mListener.OnEventCreated(token, cookie, uri);
        else if(mListener!=null&&token==ADD_REMINDER)mListener.OnReminderCreated(token,cookie,uri);
        else if(mListener!=null&&token==CREATE_CALENDAR_ID)mListener.OnCalendarCreated(token,cookie,uri);

    }

    @Override
    protected void onDeleteComplete(int token, Object cookie, int result) {
        super.onDeleteComplete(token, cookie, result);
        if(mListener!=null&&token==REMOVE_EVENT_TOKEN)mListener.OnDeleteEvent(token, cookie, result);
    }

    interface OnCalendarQueryCompletedListener{
        public void OnLoadCalendarId(int token, Object cookie, Cursor cursor);
        public void OnEventCreated(int token, Object cookie, Uri uri);
        public void OnReminderCreated(int token, Object cookie, Uri uri);
        public void OnDeleteEvent(int token, Object cookie, int result);
        public void OnCalendarCreated(int token, Object cookie, Uri uri);
    }





}
