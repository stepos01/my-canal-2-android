package com.step4fun.mycanal2.canal2.controller.networkservice;

import com.step4fun.mycanal2.canal2.model.AppInfo;
import retrofit.Callback;
import retrofit.http.GET;

import java.util.List;

/**
 * Created by sngatchou on 31/03/2015.
 */
public interface AppInfoApi {
    @GET("/appinfo/")
    public void loadAppInfos(Callback<List<AppInfo>> cb);
}
