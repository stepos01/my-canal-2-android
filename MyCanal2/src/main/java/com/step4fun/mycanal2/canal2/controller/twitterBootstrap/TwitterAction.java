package com.step4fun.mycanal2.canal2.controller.twitterbootstrap;

import android.content.Intent;
import com.step4fun.mycanal2.canal2.Utils;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.*;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

/**
 * Created by stephanekamga on 21/02/15.
 */
public abstract class TwitterAction extends Callback<TwitterSession>  {


    private static TwitterAuthClient client;
    protected  void runAction(){}
    protected  void runAction(AppSession session)
    {

    }
    private void getGuestSession()
    {
        TwitterCore.getInstance().logInGuest(new Callback<AppSession>() {
            @Override
            public void success(Result<AppSession> result) {
               AppSession guestSession = result.data;
                runAction(guestSession);
            }

            @Override
            public void failure(TwitterException exception) {
                exception.printStackTrace();
                runAction(null);
            }
        });
    }
    public void start()
    {
          if(TwitterBootstrapUtils.isUserLoggedIn())runAction();
        else{
              twitterLogin();
          }
    }
    public void start(AppSession session)
    {
        if(session == null)getGuestSession();
        else {
            runAction(session);
        }
    }

    private void twitterLogin()
    {

        client = new TwitterAuthClient();
        client.authorize(Utils.getActivity(),this);
    }



    public static void setAuthResult(int requestCode, int responseCode, Intent intent) {
        client.onActivityResult(requestCode,responseCode,intent);
    }

    @Override
    public void success(Result<TwitterSession> result) {
        Twitter.getSessionManager().setActiveSession(result.data);
        runAction();
    }

    @Override
    public void failure(TwitterException e) {
//        new SweetAlertDialog(langdetect.MyApplication.context(),SweetAlertDialog.ERROR_TYPE)
//                .setTitleText("").setContentText(langdetect.MyApplication.context().getString(R.string.twitter_login_failed)).show();
    }
}
