package com.step4fun.mycanal2.canal2.controller.twitterbootstrap;

import com.step4fun.mycanal2.canal2.controller.events.EventStub;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephanekamga on 18/03/15.
 */
public class TweetsLoadedEvent implements EventStub {
    private List<Tweet> tweetList;

    public TweetsLoadedEvent(List<Tweet> tweetList) {
        this.tweetList = tweetList;
    }

    public TweetsLoadedEvent() {
    }

    public List<Tweet> getTweetList() {
        return tweetList;
    }

    public void addTweetList(Tweet tweet) {
        if (tweetList==null)tweetList = new ArrayList<Tweet>();
        tweetList.add(tweet);
    }

}
