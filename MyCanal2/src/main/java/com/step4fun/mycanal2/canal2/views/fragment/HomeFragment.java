package com.step4fun.mycanal2.canal2.views.fragment;

import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.graphics.Point;
//import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.*;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnTouch;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.step4fun.mycanal2.canal2.MainActivity;
import com.step4fun.mycanal2.canal2.MyApplication;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.adapter.EndlessScrollListener;
import com.step4fun.mycanal2.canal2.adapter.HeaderFooterListAdapter;
import com.step4fun.mycanal2.canal2.adapter.InfiniteScrollListner;
import com.step4fun.mycanal2.canal2.adapter.PostAdapter;
import com.step4fun.mycanal2.canal2.controller.MediaPlayerControlImpl;
import com.step4fun.mycanal2.canal2.controller.alarm.SettingsContentObserver;
import com.step4fun.mycanal2.canal2.controller.events.CommentsEvent;
import com.step4fun.mycanal2.canal2.controller.events.EventStub;
import com.step4fun.mycanal2.canal2.controller.events.PostNewCommentException;

import com.step4fun.mycanal2.canal2.controller.networkservice.Service;
import com.step4fun.mycanal2.canal2.controller.twitterbootstrap.TweetAsyncLoader;
import com.step4fun.mycanal2.canal2.controller.twitterbootstrap.TweetsLoadedEvent;
import com.step4fun.mycanal2.canal2.model.Comments;
import com.step4fun.mycanal2.canal2.views.TweetDialog;
import com.step4fun.mycanal2.canal2.views.VideoControllerView;
import com.step4fun.mycanal2.canal2.views.specific.Item;
import com.step4fun.mycanal2.canal2.views.specific.ItemFactory;
import com.step4fun.mycanal2.canal2.views.specific.RowType;
import com.step4fun.mycanal2.canal2.views.specific.impl.ItemFactoryImpl;
import com.step4fun.mycanal2.canal2.views.specific.impl.RowItem;
import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.TweetViewAdapter;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.vov.vitamio.MediaPlayer;

/**
 * Created by stephanekamga on 11/01/15.
 */
public class HomeFragment extends Fragment
        implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, SurfaceHolder.Callback, MediaPlayer.OnVideoSizeChangedListener {
    private static final int MAX_COMMENTS_ALLOWED = 30;
    public static final int MAX_TWEET_ALLOWED = 30;
    public static final String STREAM_URL = "STREAM_URL";
    //    private ActionBar.Tab tab;
//
//    private ActionBar mActionBar;
//    private AlphaAnimation fadeIn;
//    private AlphaAnimation fadeOut;
    private Handler mHandler = new Handler();
    private SweetAlertDialog dialog;
    MediaPlayer player;
    private TweetViewAdapter<CompactTweetView> adapter;
    SettingsContentObserver mSettingsContentObserver;
    VideoControllerView controller;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @InjectView(R.id.drawerContent)
    LinearLayout drawerContent;
    @InjectView(R.id.videoViewContainer)
    View mVideoViewContainer;
    //    @InjectView(R.id.centralPlayButton)
//    ImageView centralPlayButton;
    @InjectView(R.id.videoSurface)
    SurfaceView videoSurface;
    @InjectView(R.id.tweet_refresh_container)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @InjectView(R.id.controller_holder)
    FrameLayout controllerHolder;
    @InjectView(R.id.tweet_list)
    ListView tweetList;
    @InjectView(R.id.home_list_id)
    ListView postList;
    @InjectView(R.id.home)
    RelativeLayout home;
    @InjectView(R.id.swipe_refresh_layout_id)
    SwipyRefreshLayout pullLayout;
    private PostAdapter postAdapter;
    private Service service;
    private HeaderFooterListAdapter<PostAdapter> mAdapter;
    private View footerView;

    private Callback<String> cb = new Callback<String>() {
        @Override
        public void success(String result, Response response) {
            if (!Utils.isNullOrEmpty(result)) {
                try {
                    //   player.setAudioAmplify(AudioManager.STREAM_MUSIC);
                    player.setDataSource(getActivity(), Uri.parse(result));
                    player.setOnPreparedListener(HomeFragment.this);
                    player.setOnVideoSizeChangedListener(HomeFragment.this);
                    player.setOnErrorListener(HomeFragment.this);
                    player.prepareAsync();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                onError(player, 0, 0);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            onError(player, 0, 0);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
//        // Activate Navigation Mode Tabs
//        mActionBar = getSherlockActivity().getSupportActionBar();
//        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        service = Service.getInstance();
        setRetainInstance(true);
//        mProvider = new ApiStreamInfoProvider(getActivity());
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            home.setBackgroundResource(android.R.color.transparent);
            postList.setVisibility(View.GONE);
            ((MainActivity) getActivity()).getSupportActionBar().hide();
        } else {
            home.setBackgroundResource(android.R.color.white);
            postList.setVisibility(View.VISIBLE);
            ((MainActivity) getActivity()).getSupportActionBar().show();
        }
        //  getView().invalidate();
        super.onConfigurationChanged(newConfig);
        setVideoSurfaceDim();
    }
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(MainActivity.CURRENT_FRAGMENT_KEY, MainActivity.LiveFragmentTag);
    }
    public void setVideoSurfaceDim() {
        int videoWidth = player.getVideoWidth();
        int videoHeight = player.getVideoHeight();
        float videoProportion = (float) videoWidth / (float) videoHeight;
        int screenWidth = 0, screenHeight = 0;
        // Get the width of the screen
        final WindowManager windowManager = getActivity().getWindowManager();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            final Point size = new Point();
            windowManager.getDefaultDisplay().getSize(size);
            screenHeight = size.y;
            screenWidth = size.x;
        } else {
            Display d = windowManager.getDefaultDisplay();
            screenWidth = d.getWidth();
            screenHeight = d.getHeight();
        }
//        screenHeight = getDisplayContentHeight();
        float screenProportion = (float) screenWidth / (float) screenHeight;
        // Get the SurfaceView layout parameters
        ViewGroup.LayoutParams lp = videoSurface.getLayoutParams();
        if (videoProportion > screenProportion) {
            lp.width = screenWidth;
            lp.height = (int) ((float) screenWidth / videoProportion);
        } else {
            lp.width = (int) (videoProportion * (float) screenHeight);
            lp.height = screenHeight;
        }
        // Commit the layout parameters
        videoSurface.setLayoutParams(lp);
    }

    public void initVolumeObserver() {


        mSettingsContentObserver = new SettingsContentObserver(getActivity(), mHandler);
        getActivity().getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mSettingsContentObserver);
        mSettingsContentObserver.addListener(new SettingsContentObserver.VolumeChangedListener() {
            @Override
            public void VolumeChanged(int newVolume) {
                if(controller!=null)controller.switchedVolumeView(newVolume);
            }
        });
    }

    public void initSurfaceView() {
        videoSurface.getHolder().addCallback(this);
        videoSurface.getHolder().setFormat(PixelFormat.RGBA_8888);
    }

    @OnTouch(R.id.videoSurface)
    public boolean onTouchScreen(View view, MotionEvent motionEvent) {
        controller.show();
        return false;
    }

//    @OnTouch(R.id.videoViewContainer)
//    public boolean onTouchScreen(View view, MotionEvent motionEvent) {
//        controller.show();
//        return false;
//    }

    public void initDialog() {
        dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE)
                .setContentText(getResources().getString(R.string.obtaining_stream))
                .setTitleText("");
        dialog.setCanceledOnTouchOutside(false);
//        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                mProvider.setOnLoadedListener(null);
//            }
//        });
        dialog.show();
    }

//    @OnClick(R.id.centralPlayButton)
//    public void onClickOnCentralPlayButton(View v) {
//        v.setVisibility(View.GONE);
//    }

    public void initDrawerTweetList() {
        adapter = new TweetViewAdapter<CompactTweetView>(
                getActivity());
        tweetList.setAdapter(adapter);
        new TweetAsyncLoader().execute((long) MAX_TWEET_ALLOWED);
        initScrollListener();
    }

    public void initScrollListener()
    {
        tweetList.setOnScrollListener(new EndlessScrollListener(adapter,mSwipeRefreshLayout));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.home, container, false);
        ButterKnife.inject(this, convertView);
        controller = new VideoControllerView(getActivity());
        controller.setListenerOnTwitterIconClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(drawerContent);
                controller.hide();
            }
        });
        controller.setListenerOnPenIconClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TweetDialog().init((MainActivity) getActivity()).show();
            }
        });


        initSurfaceView();

        initDialog();

        initDrawerTweetList();

        initPostList(inflater, container);

//        initPostList();

        enableListener();

        initRefreshView();

        initVolumeObserver();
        return convertView;
    }

    private void enableListener() {
        final ItemFactory<Comments> itemFactory = new ItemFactoryImpl<Comments>();
        pullLayout.setOnRefreshListener(new MyRefreshListener(service, itemFactory, mAdapter.getWrappedAdapter(), pullLayout));
        postList.setOnScrollListener(new MyEndlessScrollListener(service, footerView, mAdapter));
    }

    private void initPostList(LayoutInflater inflater, ViewGroup container) {
        footerView = inflater.inflate(R.layout.footer, postList, false); // We retrieve the footer view
        final ItemFactory<Comments> itemFactory = new ItemFactoryImpl<Comments>();
        final List<Item> data = new ArrayList<Item>();
        data.addAll(headersItems(itemFactory));
        postAdapter = new PostAdapter(getActivity(), data);
        mAdapter = new HeaderFooterListAdapter<PostAdapter>(postList, postAdapter);
        postList.setAdapter(mAdapter);
        this.mAdapter.addFooter(footerView); // We add the footer
        service.getAllComments(new CallbackImpl(itemFactory, mAdapter.getWrappedAdapter(), EventType.SIMPLE.ordinal()));
    }

//    private void initPostList() {
//        final ItemFactory<Comments> itemFactory = new ItemFactoryImpl<Comments>();
//        final List<Item> data = new ArrayList<Item>();
//        data.add(itemFactory.createItem(null, RowType.PUB.getType()));
//        data.add(itemFactory.createItem(null, RowType.HEADER.getType()));
//        postAdapter = new PostAdapter(getActivity(), data);
//        postList.setAdapter(postAdapter);
//        service.getAllComments(new CallbackImpl(itemFactory, postAdapter, EventType.SIMPLE.ordinal()));
//    }

    private List<Item> createItems(ItemFactory<Comments> itemFactory, List<Comments> comments) {
        final List<Item> data = new ArrayList<Item>();
        for (Comments comment : comments) data.add(itemFactory.createItem(comment, RowType.ROW.getType()));

        return data;
    }

    private void initRefreshView() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new TweetAsyncLoader().execute((long)MAX_TWEET_ALLOWED);
            }
        });
    }

    private void getStreamUrl() {
        if (getArguments().getString(STREAM_URL) == null) {
            Service.getInstance().getStreamUrl(cb);
        } else {
            cb.success(getArguments().getString(STREAM_URL), null);
        }
    }

    private void initPlayer(SurfaceHolder holder) {
        player = new MediaPlayer(getActivity());
//        player = new MediaPlayer();
        player.setOnPreparedListener(this);
        player.setOnErrorListener(this);
        player.setDisplay(holder);
        getStreamUrl();
    }

    private List<Item> itemsForErrorOrNoComments(ItemFactory itemFactory, String eventType) {
        final List<Item> items = new ArrayList<Item>();
        items.addAll(headersItems(itemFactory));
        if (eventType.equals(RowType.ERROR.getType())) {
            items.add(itemFactory.createItem(null, RowType.ERROR.getType()));
        } else if (eventType.equals(RowType.NO_COMMENTS.getType())) {
            items.add(itemFactory.createItem(null, RowType.NO_COMMENTS.getType()));
        }

        return items;
    }

    private List<Item> headersItems(ItemFactory itemFactory) {
        final List<Item> items = new ArrayList<Item>();
        items.add(itemFactory.createItem(null, RowType.PUB.getType()));
        items.add(itemFactory.createItem(null, RowType.HEADER.getType()));

        return items;
    }

    private void fireDataRefresh(List<Item> items, int eventType, PostAdapter adapter) {
        switch (eventType) {
            case 0:
                // Natural load from server
                adapter.swapItems(items);
                break;
            case 1:
                // Pull to refresh action
                adapter.setElements(items);
                break;
            case 2:
                // There is an error while making network request
                adapter.errorItems(items);
                break;
            case 3:
                // There is no comment
                adapter.noCommentsItems(items);
                break;
            case 4:
                // Load from an OnScroll action
                adapter.addNewItems(items);
                break;
        }
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {

        player.pause();

        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();

    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
//        mp.(AudioManager.STREAM_MUSIC);
        controller.setMediaPlayer(new MediaPlayerControlImpl(mp));
        controller.setAnchorView(controllerHolder);
        dialog.dismiss();
        mp.start();
    }

    @Override
    public boolean onError(final MediaPlayer mp, int what, int extra) {
       if(this.isVisible())
       {
           dialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
           dialog.setContentText(getResources().getString(R.string.oops))
                   .showCancelButton(true)
                   .setCancelText(getResources().getString(R.string.error_dialog_cancel))
                   .setConfirmText(getResources().getString(R.string.error_dialog_retry))
                   .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                       @Override
                       public void onClick(SweetAlertDialog sweetAlertDialog) {
                           sweetAlertDialog.dismiss();

                       }
                   })
                   .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                       @Override
                       public void onClick(SweetAlertDialog sweetAlertDialog) {
                           sweetAlertDialog.setContentText(getResources().getString(R.string.loading));
                           Service.getInstance().getStreamUrl(cb);
                           sweetAlertDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                       }
                   });
           dialog.setCanceledOnTouchOutside(false);
           dialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
           dialog.show();

       }
        return true;

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        initPlayer(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        player.reset();
        player.release();
        player = null;
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        if (getActivity()!=null)setVideoSurfaceDim();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    public void onEventMainThread(EventStub event) {
        if (event instanceof TweetsLoadedEvent) {
            mSwipeRefreshLayout.setRefreshing(false);
            adapter.setTweets(((TweetsLoadedEvent) event).getTweetList());
            adapter.notifyDataSetChanged();
        } else if (event instanceof CommentsEvent) {
            ItemFactory<Comments> itemFactory = new ItemFactoryImpl<Comments>();
            final List<Item> items = new ArrayList<Item>();
            items.add(itemFactory.createItem(((CommentsEvent) event).getComment(), RowType.ROW.getType()));
            postAdapter.swapItems(items);
        } else if (event instanceof PostNewCommentException) {
            // Retrieve the last comment inserted
            postAdapter.removeItems(2);
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        MyApplication.context().getContentResolver().unregisterContentObserver(mSettingsContentObserver);
//        mSettingsContentObserver.addListener(null);
//        mSettingsContentObserver.addListener(new SettingsContentObserver.VolumeChangedListener() {
//            @Override
//            public void VolumeChanged(int newVolume) {
//                if(controller!=null)controller.switchedVolumeView(newVolume);
//            }
//        });
        super.onStop();
    }


    private enum EventType {
        SIMPLE, REFRESH, ERROR, NO_COMMENT, SCROLL
    }

    private class CallbackImpl implements Callback<List<Comments>> {
        private final ItemFactory<Comments> itemFactory;
        private final PostAdapter adapter;
        private final int eventType;

        public CallbackImpl(ItemFactory<Comments> itemFactory, PostAdapter adapter,
                            int eventType) {
            this.itemFactory = itemFactory;
            this.adapter = adapter;
            this.eventType = eventType;
        }

        /**
         * Successful HTTP response.
         *
         * @param comments
         * @param response
         */
        @Override
        public void success(List<Comments> comments, Response response) {
            final List<Item> items = new ArrayList<Item>();
            if (comments == null || comments.size() == 0) {
                items.addAll(itemsForErrorOrNoComments(itemFactory,
                        RowType.NO_COMMENTS.getType()));
                fireDataRefresh(items, EventType.NO_COMMENT.ordinal(), adapter);
            } else {
                items.addAll(createItems(itemFactory, comments));
                fireDataRefresh(items, eventType, adapter);
            }
        }

        /**
         * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
         * exception.
         *
         * @param error
         */
        @Override
        public void failure(RetrofitError error) {
            //TODO: Deal with error message in a smarter way
            // We don't show the list
            fireDataRefresh(itemsForErrorOrNoComments(itemFactory,
                    RowType.ERROR.getType()), EventType.ERROR.ordinal(), adapter);
        }
    }

    private class MyRefreshListener implements SwipyRefreshLayout.OnRefreshListener {
        private final Service service;
        private final ItemFactory<Comments> itemFactory;
        private final PostAdapter adapter;
        private final SwipyRefreshLayout pullLayout;

        public MyRefreshListener(Service service, ItemFactory<Comments> itemFactory,
                                 PostAdapter adapter, SwipyRefreshLayout pullLayout) {
            this.service = service;
            this.itemFactory = itemFactory;
            this.adapter = adapter;
            this.pullLayout = pullLayout;
        }

        @Override
        public void onRefresh(SwipyRefreshLayoutDirection direction) {
            service.getAllComments(new Callback<List<Comments>>() {
                @Override
                public void success(List<Comments> comments, Response response) {
                    if (comments != null || comments.size() > 0) {
                        final List<Item> items = new ArrayList<Item>();
                        items.addAll(headersItems(itemFactory));
                        items.addAll(createItems(itemFactory, comments));
                        fireDataRefresh(items, EventType.REFRESH.ordinal(), adapter);
                    }
                    pullLayout.setRefreshing(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    fireDataRefresh(itemsForErrorOrNoComments(itemFactory,
                            RowType.ERROR.getType()), EventType.ERROR.ordinal(), adapter);
                    pullLayout.setRefreshing(false);
                }
            });
        }
    }


    private class MyEndlessScrollListener extends InfiniteScrollListner {
        private final Service service;
        private final View footerView;
        private final HeaderFooterListAdapter<PostAdapter> adapter;

        public MyEndlessScrollListener(Service service, View footerView, HeaderFooterListAdapter adapter) {
            this.service = service;
            this.footerView = footerView;
            this.adapter = adapter;
        }

        @Override
        protected void onLoadMore(int offset, int totalItemCount) {
            customLoadMoreDataFromApi(totalItemCount);
        }

        private void customLoadMoreDataFromApi(int totalItemCount) {
            final List<Item> items = adapter.getWrappedAdapter().getElements();
            final int totalItems = adapter.getWrappedAdapter().getCount();
            final Item item = items.get(totalItems - 1);
            if (item instanceof RowItem) {
                final RowItem<Comments> row = (RowItem<Comments>) item;
                service.getAllComments(new Callback<List<Comments>>() {
                    @Override
                    public void success(List<Comments> comments, Response response) {
                        if (comments != null) {
                            if (comments.size() == 0) {
                                adapter.removeFooter(footerView);
                            } else {
                                fireDataRefresh(createItems(new ItemFactoryImpl(), comments),
                                        EventType.SCROLL.ordinal(), adapter.getWrappedAdapter());
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                    /* Remove the footer from the List */
                        adapter.removeFooter(footerView);
                    }
                }, row.getElement().getDateTime(), MAX_COMMENTS_ALLOWED);
            }
        }
    }
}

