package com.step4fun.mycanal2.canal2.controller.events;

import com.step4fun.mycanal2.canal2.model.Comments;

/**
 * Created by stephanekamga on 20/03/15.
 */
public class PostNewCommentException extends Exception
        implements EventStub {
    private Comments comments;

    public PostNewCommentException(Comments comments) {
        this.comments = comments;
    }

    public Comments getComments() {
        return comments;
    }
}
