package com.step4fun.mycanal2.canal2.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by stephanekamga on 28/01/15.
 */
public class ShowsDetailsModel implements Parcelable {
    private ArrayList<String> imageUrlList;
    private String videoId;
    private String title;
    private String description;
    private String time;
    private String section;
    private boolean isPlayList;
    private String duration;

    /**
     *   Android specific
     */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(imageUrlList);
        dest.writeString(videoId);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(time);
        dest.writeInt(isPlayList ? 1 : -1);
        dest.writeString(section);


    }

    public static final Creator<ShowsDetailsModel> CREATOR
            = new Creator<ShowsDetailsModel>() {
        public ShowsDetailsModel createFromParcel(Parcel in) {
            return new ShowsDetailsModel(in);
        }

        public ShowsDetailsModel[] newArray(int size) {
            return new ShowsDetailsModel[size];
        }
    };

    private ShowsDetailsModel(Parcel in) {
        in.readStringList(imageUrlList);
        videoId = in.readString();
        title = in.readString();
        description = in.readString();
        time = in.readString();
        isPlayList = in.readInt() == 1;
        section = in.readString();
    }

    /**
     *   end Android shit
     */

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Boolean getIsPlayList() {
        return isPlayList;
    }

    public void setIsPlayList(Boolean isPlayList) {
        this.isPlayList = isPlayList;
    }

    public ArrayList<String> getImageUrl() {
        return imageUrlList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String mTitle) {
        this.title = mTitle;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String mTime) {
        this.time = mTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String mDescription) {
        this.description = mDescription;
    }

    public void setImageUrl(ArrayList<String> mImageUrk) {
        this.imageUrlList = mImageUrk;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public ShowsDetailsModel() {
    }

    public ShowsDetailsModel(String videoid, ArrayList<String> mImageUrlList) {
        this.videoId = videoid;
        this.imageUrlList = mImageUrlList;
    }

    public ShowsDetailsModel(ArrayList<String> mImageUrlList) {
        this.imageUrlList = mImageUrlList;
    }


    public ShowsDetailsModel(String description, ArrayList<String> mImageUrlList, String time, String title, String videoId) {
        this.description = description;
        this.imageUrlList = mImageUrlList;
        this.time = time;
        this.title = title;
        this.videoId = videoId;
    }

    public ShowsDetailsModel(String title, String description, ArrayList<String> mImageUrlList, String time) {
        this.title = title;
        this.description = description;
        this.imageUrlList = mImageUrlList;
        this.time = time;
    }


    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }
}
