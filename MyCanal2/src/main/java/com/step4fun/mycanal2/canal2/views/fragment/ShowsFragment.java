package com.step4fun.mycanal2.canal2.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.AdapterView;
import android.widget.SearchView;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.step4fun.mycanal2.canal2.MainActivity;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.adapter.ShowListViewAdapter;
import com.step4fun.mycanal2.canal2.controller.networkservice.Service;
import com.step4fun.mycanal2.canal2.controller.suggestion.ShowSuggestionController;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stephanekamga on 24/01/15.
 */
public class ShowsFragment extends Fragment implements  AdapterView.OnItemClickListener {
    private StickyListHeadersListView mListView;
    private ShowListViewAdapter mAdapter;
    private String TAG = getClass().getSimpleName();
    public static final int VIEW_TYPE_ADS = 2;
    public static final int VIEW_TYPE_SHOW = 1;
    public static final int VIEW_TYPE_HEADER = 3;
    public static final String SHOWS_LIST = "SHOWS_LIST";
    private SweetAlertDialog mDialog;
    private ShowSuggestionController mShowSuggestionController;
//    private ProgressWheel mWheel;
    private static final String SHOWS_FRAGMENT = "SHOWS_FRAGMENT";
    private List<ProgramsModel> mModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mContainer = inflater.inflate(R.layout.shows, container, false);
        mListView = (StickyListHeadersListView) mContainer.findViewById(R.id.shows_stickyList);
        mListView.setVisibility(View.GONE);
        mListView.setOnItemClickListener(this);

        return mContainer;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.show_fragment_menu, menu);
        SearchView mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mShowSuggestionController = ShowSuggestionController.getInstance(getActivity(), mSearchView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDialog = new SweetAlertDialog(((MainActivity) getActivity()).getSupportActionBar().getThemedContext(), SweetAlertDialog.PROGRESS_TYPE)
                .setContentText(getResources().getString(R.string.loading)).setTitleText("");
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(true);
        mDialog.show();
        if (mModel != null) {
            setUpadapter(new ArrayList<ProgramsModel>(mModel));
            return;
        }
        Service.getInstance().getShows(new Callback<List<ProgramsModel>>() {
            @Override
            public void success(List<ProgramsModel> modelList, Response response) {
                if (modelList == null) {
                    if (mDialog.isShowing()) {
                        mDialog.setTitleText(getResources().getString(R.string.oops))
                                .setContentText(getResources().getString(R.string.error_loading))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        ((MainActivity) getActivity()).LaunchPrincipal(null);
                                    }
                                })
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    } else {
                        mDialog = new SweetAlertDialog(getThemeContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(getResources().getString(R.string.oops))
                                .setContentText(getResources().getString(R.string.error_loading));
                        mDialog.show();
                    }
                } else {
//                    mModels = generateListItems((ArrayList<ProgramsModel>) modelList);
                    setUpadapter(new ArrayList<ProgramsModel>(modelList));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (mDialog.isShowing()) {
                    mDialog.setTitleText(getResources().getString(R.string.oops))
                            .setContentText(getResources().getString(R.string.error_loading))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    ((MainActivity) getActivity()).LaunchPrincipal(null);
                                }
                            })
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                } else {
                    mDialog = new SweetAlertDialog(getThemeContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getResources().getString(R.string.oops))
                            .setContentText(getResources().getString(R.string.error_loading));
                    mDialog.show();
                }
            }
        });
    }

    public void setUpadapter(List<ProgramsModel>model)
    {
        mModel = model;
        mAdapter = new ShowListViewAdapter(new ArrayList<ProgramsModel>(mModel));
        EventBus.getDefault().register(mAdapter);
        mListView.setAdapter(mAdapter);
        mDialog.dismiss();

        mListView.setVisibility(View.VISIBLE);
        mShowSuggestionController.Build(new ArrayList<ProgramsModel>(mModel), mAdapter);

    }

    @Override
    public void onDestroy() {

        if(mAdapter!=null&&EventBus.getDefault().isRegistered(mAdapter))EventBus.getDefault().unregister(mAdapter);
        super.onDestroy();
    }

    private Context getThemeContext()
    {
        return ((MainActivity)getActivity()).getSupportActionBar().getThemedContext();
    }

//    private ArrayList<ProgramsModel> generateListItems(List<ProgramsModel> src)
//    {
//        HashMap<String,ArrayList<ProgramsModel>> map = new HashMap<String,ArrayList<ProgramsModel>>();
//        for(ProgramsModel model : src)
//        {
//            if(map.containsKey(model.getSection())){
//                ArrayList old = new ArrayList<ProgramsModel>(map.get(model.getSection()));
//                old.add(model);
//                map.put(model.getSection(),old);
//            }
//            else map.put(model.getSection(), new ArrayList<ProgramsModel>(Arrays.asList(model)));
//        }
//        ArrayList<ProgramsModel> result = new ArrayList<ProgramsModel>();
//        for(String key: map.keySet())
//        {
//            result.add(new ProgramsModel(key,VIEW_TYPE_HEADER));
//            for(ProgramsModel m : map.get(key))
//            {
//                m.setType(VIEW_TYPE_SHOW);
//                result.add(m);
//            }
//        }
//        return result;
//    }

//    @Override
//    public <T> void onLoadComplete(T modelList) {
//        if (modelList == null)
//        {
//            if(mDialog.isShowing())
//            {
//                  mDialog.setTitleText("Oops...")
//                    .setContentText(getResources().getString(R.string.error_loading))
//                          .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                              @Override
//                              public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                  sweetAlertDialog.dismiss();
//                                  ((MainActivity)getActivity()).LaunchPrincipal();
//                              }
//                          })
//                    .changeAlertType(SweetAlertDialog.ERROR_TYPE);
//            }else{
//                mDialog = new SweetAlertDialog(getThemeContext(), SweetAlertDialog.ERROR_TYPE)
//                        .setTitleText("Oops...")
//                        .setContentText(getResources().getString(R.string.error_loading));
//                        mDialog.show();
//            }
//        } else
//        {
//            mModels = generateListItems((ArrayList<ProgramsModel>) modelList);
//            mAdapter = new ShowListViewAdapter(getThemeContext(),R.layout.shows_item,new ArrayList<ProgramsModel>(mModels));
//            mListView.setAdapter(mAdapter);
//            mDialog.dismiss();
//            mListView.setVisibility(View.VISIBLE);
//            mShowSuggestionController.InitController(mModels,mAdapter);
//        }
//    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(MainActivity.CURRENT_FRAGMENT_KEY, MainActivity.ShowsFragmentTag);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ProgramsModel toSend = null;
//        try {
        toSend = (ProgramsModel) mAdapter.getItem(position);
//            Service.getInstance().incrementViews(toSend);
//
//            ProgramsModel temp = (ProgramsModel) mAdapter.getItem(position);
//            temp.setViewsCount(temp.getViewsCount() + 1);
//            EventBus.getDefault().post(new ProgramModelChangedEvent(temp));

//        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//        ft.hide(ShowsFragment.this).add(R.id.fragmentContainer, detailsFragmentContainer, DETAILS_FRAGMENT).addToBackStack("").commit();
////        } catch (CloneNotSupportedException e) {
////            e.printStackTrace();
////        }

        ((MainActivity) getActivity()).LaunchProgramDetails(toSend);
    }
}
