package com.step4fun.mycanal2.canal2.views.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import com.bluejamesbond.text.DocumentView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.step4fun.mycanal2.canal2.BaseActivity;
import com.step4fun.mycanal2.canal2.MainActivity;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.controller.events.ProgramModelChangedEvent;
import com.step4fun.mycanal2.canal2.controller.networkservice.Service;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;
import com.step4fun.mycanal2.canal2.model.ShowsVideo;
import com.step4fun.mycanal2.canal2.model.UserShowPreference;
import de.greenrobot.event.EventBus;

/**
 * Created by stephanekamga on 28/01/15.
 */
public class ShowDetailsFragmentContainer extends Fragment {
    //    private ViewPager mViewPager;
//    private ViewPagerAdapter mAdapter;
//    private LinePageIndicator mPageIndicator;
//    private TextView mTitle;
    //    private TextView mDescription;
//    private DocumentView mDescriptionJust;
//    private CalendarService calendarService;
    //    private MenuItem alarmMenu;
    private static final String YOUTUBE_API_KEY = "AIzaSyAw4gpoIJzQ3SDQPx4NcbjMGvhxSwbo5eg";
    public static final String PROGRAM_MODEL_PARCEL_KEY = "PROGRAM_MODEL_PARCEL_KEY";
    private  ProgramsModel model;


    private SweetAlertDialog pDialog;

    //    @InjectView(R.id.shows_details_time)
//    TextView mTime;
//    @InjectView(R.id.shows_details_duration)
//    TextView mDuration;
//    @InjectView(R.id.shows_details_type)
//    TextView mType;
//    @InjectView(R.id.show_details_like_not_clicked_id)
//    ImageButton mNotLike;
    @InjectView(R.id.show_details_image)
    ImageView image;
    @InjectView(R.id.shows_details_description)
    DocumentView mDescriptionJust;
    @InjectView(R.id.show_details_like)
    ImageButton likeButton;
    @InjectView(R.id.shows_details_title)
    TextView mTitle;
    @InjectView(R.id.show_details_play_button)
    ImageView playButton;
//    ImageButton mLike;

    public static ShowDetailsFragmentContainer getInstance(ProgramsModel modelbig) {
        ShowDetailsFragmentContainer fragmentContainer = new ShowDetailsFragmentContainer();
        Bundle b = new Bundle();
        b.putParcelable(PROGRAM_MODEL_PARCEL_KEY, modelbig);
        fragmentContainer.setArguments(b);
        return fragmentContainer;
    }

    public static ShowDetailsFragmentContainer getInstance(Bundle b) {
        ShowDetailsFragmentContainer fragmentContainer = new ShowDetailsFragmentContainer();
        fragmentContainer.setArguments(b);
        return fragmentContainer;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null)
        {
           model = savedInstanceState.getParcelable(PROGRAM_MODEL_PARCEL_KEY);
        }
        setHasOptionsMenu(true);
        setRetainInstance(true);
//        (MainActivity(Utils.getActivity())).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public ProgramsModel getModel() {
        if(model==null)
            model= getArguments().getParcelable(PROGRAM_MODEL_PARCEL_KEY);
        return model;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

//        calendarService = new CalendarService((MainActivity) getActivity());

        View view = inflater.inflate(R.layout.shows_details, container, false);
//        mViewPager = (ViewPager) view.findViewById(R.id.shows_details_pager);

//        mAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
//        mViewPager.setOffscreenPageLimit(3);
//        mViewPager.setAdapter(mAdapter);

//        mPageIndicator = (LinePageIndicator) view.findViewById(R.id.shows_details_pager_indicator);
//        mPageIndicator.setViewPager(mViewPager);

        //mPageIndicator.setOnPageChangeListener(this);

        ButterKnife.inject(this, view);

//        mDescription = (TextView)view.findViewById(R.id.shows_details_description);
//        mDescriptionJust = (DocumentView) view.findViewById(R.id.shows_details_description_just);
//        mTitle = (TextView) view.findViewById(R.id.shows_details_title);
//        mType.setText(model.getSection());
//        mTime.setText(model.getStartTimeToString());
        mTitle.setText(getModel().getTitle());
//        mDescription.setText(model.getSynopsis());
        mDescriptionJust.setText(getModel().getSynopsis());
//        mDuration.setText(model.getDurationToString());
        loadLikeButton();


        if (getModel().getShowsVideos() == null || getModel().getShowsVideos().size() == 0)
        {
            //we show the default image
            Utils.load(getActivity(), getModel().getImageUrl(), image);
            playButton.setVisibility(View.GONE);
        }else{
            //there are some video
            Utils.load(getActivity(), pickTheVideoUrl().getImageUrl(), image);
            playButton.setVisibility(View.VISIBLE);

        }
        return view;
    }

    public ShowsVideo pickTheVideoUrl() {
        for (ShowsVideo showsVideo : getModel().getShowsVideos())
        {
            if (showsVideo.isPlaylist())
            {
                return showsVideo;
            }
        }
        return getModel().getShowsVideos().get(0);
    }

    private void loadLikeButton() {
        if (UserShowPreference.getInstance(getActivity()).isPreferredShow(getModel()))
            likeButton.setImageDrawable(getResources().getDrawable(R.drawable.like_red));
        else
            likeButton.setImageDrawable(getResources().getDrawable(R.drawable.like_blue));
    }

    @OnClick(R.id.show_details_like)
    public void switchLike() {
        UserShowPreference.getInstance(getActivity()).switchPreference(getModel());
        if (UserShowPreference.getInstance(getActivity()).isPreferredShow(getModel()))
        {
            Service.getInstance().incrementLikes(getModel());
        }else{
            Service.getInstance().decrementLikes(getModel());
        }
        try {
            ProgramsModel temp = (ProgramsModel) getModel().clone();
            temp.setLikesCount(temp.getLikesCount() + 1);
            EventBus.getDefault().post(new ProgramModelChangedEvent(temp));

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        loadLikeButton();
    }

    @OnClick(R.id.show_details_play_button)
    public void launchVideo() {

        if (playButton.getVisibility() == View.VISIBLE) {

            Intent intent = null;
            try {
                ProgramsModel temp = (ProgramsModel) getModel().clone();
                Service.getInstance().incrementViews(temp);

            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            getModel().setViewsCount(getModel().getViewsCount() + 1);
            EventBus.getDefault().post(new ProgramModelChangedEvent(getModel()));

            ShowsVideo showsVideo = pickTheVideoUrl();
            if (Utils.canUseYoutube()) {
                if (!Utils.isNullOrEmpty(showsVideo.getVideoId()) && showsVideo.isPlaylist) {
                    intent = YouTubeStandalonePlayer.createPlaylistIntent(getActivity(), YOUTUBE_API_KEY,
                            showsVideo.getVideoId(), 0, 0, true, false);
                } else if (!Utils.isNullOrEmpty(showsVideo.getVideoId()) && !showsVideo.isPlaylist) {
                    intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(),
                            YOUTUBE_API_KEY, showsVideo.getVideoId(), 0, true, false);
                }
                if (intent != null) startActivity(intent);

            } else {
                if (!Utils.isNullOrEmpty(showsVideo.getVideoId()) && !showsVideo.isPlaylist) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + showsVideo.videoId)));
                } else if (!Utils.isNullOrEmpty(showsVideo.getVideoId()) && showsVideo.isPlaylist) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/playlist?list=" + showsVideo.videoId)));

                }
            }

        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.show_fragment_details_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
//        alarmMenu = menu.getItem(1);
//        if (model.isTimeSet()) {
//            updateAlarmIcon();
//        } else {
//            alarmMenu.setVisible(false);
//        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(MainActivity.CURRENT_FRAGMENT_KEY, MainActivity.ShowDetailFragmentTag);
        outState.putParcelable(PROGRAM_MODEL_PARCEL_KEY,model);
    }
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        if (savedInstanceState != null&&MainActivity.ShowDetailFragmentTag.equals(savedInstanceState.getString(MainActivity.ShowDetailFragmentTag))) {
//            //
//
//        }
//    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            String shareText = "I'm watching " + getModel().getTitle() + " using " + getResources().getString(R.string.app_name) + " App for Android";
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_to)));
            return true;
//        } else if (item.getItemId() == R.id.action_reminder) {
//            if (!CalendarService.canUseCalendarICS()) {
//                pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
//                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
//                pDialog.setContentText(getResources().getString(R.string.calendar_not_present));
//                pDialog.setCancelable(true);
//                pDialog.show();
//            }
//            pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
//            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
//            pDialog.setTitleText(getResources().getString(R.string.loading));
//            pDialog.setCancelable(true);
//            pDialog.show();
////            calendarService.setReminderServiceNotification(this);
//            if (calendarService.hasReminder(model.getId())) calendarService.removeReminder(model);
//            else calendarService.addReminder(model);
        }
        return super.onOptionsItemSelected(item);
    }


//    @Override
//    public void notifySender(boolean isSuccess) {
//        String text = isSuccess ? getResources().getString(R.string.ok) : getResources().getString(R.string.fail);
//        pDialog.setContentText(text).changeAlertType(isSuccess ? SweetAlertDialog.SUCCESS_TYPE : SweetAlertDialog.ERROR_TYPE);
//        if (!pDialog.isShowing()) pDialog.show();
//        updateAlarmIcon();
//    }

//    private void updateAlarmIcon() {
//        if (calendarService.hasReminder(model.getId())) {
//            alarmMenu.setIcon(getResources().getDrawable(R.drawable.reminders));
//        } else {
//            alarmMenu.setIcon(getResources().getDrawable(R.drawable.no_reminders));
//        }
//    }

//    class ViewPagerAdapter extends FragmentStatePagerAdapter {
//
//        public ViewPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int i) {
//            if (i == 0) {
//                return ShowDetailsHeaderFragment.getFragmentInstance(model.getImageUrl(), null);
//            }
//            return ShowDetailsHeaderFragment.getFragmentInstance(model.getImageUrl(),
//                    model.getShowsVideos().get(i - 1));
//        }
//
//        @Override
//        public int getCount() {
//            return 1 + (model.getShowsVideos() == null ? 0 : model.getShowsVideos().size());
//        }
//    }
}

