package com.step4fun.mycanal2.canal2.controller.twitterbootstrap;

import android.app.Activity;
import android.os.AsyncTask;
import com.cybozu.labs.langdetect.LangDetectException;
import com.cybozu.labs.langdetect.Language;
import com.step4fun.mycanal2.canal2.MyApplication;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.controller.LanguageChecker;
import com.step4fun.mycanal2.canal2.views.fragment.HomeFragment;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.*;
import com.twitter.sdk.android.core.internal.TwitterApiConstants;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;
import de.greenrobot.event.EventBus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by stephanekamga on 21/02/15.
 */
public class TweetAsyncLoader extends AsyncTask<Long, Void, Void> {
    private String mCanal2Tag = MyApplication.context().getResources().getString(R.string.canal2tag);
    private String mMyCanal2Tag = MyApplication.context().getResources().getString(R.string.mycanal2tag);
    private String RESULT_TYPE = "recent";
    private AppSession appSession;

    private TweetAsyncLoader(Activity context, String tag) {
        super();
        mCanal2Tag = tag;
    }

    public TweetAsyncLoader() {
    }

    public void newTweets(final List<Tweet> list) {
        if (!list.isEmpty()) {

            TweetsLoadedEvent stickyEvent = EventBus.getDefault().getStickyEvent(TweetsLoadedEvent.class);
            Set<Tweet> copy = new HashSet<Tweet>();
            if (stickyEvent == null) copy.addAll(list);
            else {
                copy.addAll(stickyEvent.getTweetList());
                copy.addAll(list);
            }

            EventBus.getDefault().postSticky(new TweetsLoadedEvent(new ArrayList<Tweet>(copy)));
        }

    }

    public void newTweets(Tweet tweet) {
        TweetsLoadedEvent stickyEvent = EventBus.getDefault().getStickyEvent(TweetsLoadedEvent.class);
        List<Tweet> copy = new ArrayList<Tweet>();
        copy.add(tweet);
        copy.addAll(0, stickyEvent == null ? new ArrayList<Tweet>() : stickyEvent.getTweetList());
        EventBus.getDefault().postSticky(new TweetsLoadedEvent(copy));
    }

    @Override
    protected Void doInBackground(Long... longs) {
        final int count = longs[0].intValue();
         long max_id = 0;
        if(longs.length>1) max_id  = longs[1];
        final long finalMax_id = max_id;
        new TwitterAction() {
            @Override
            protected void runAction(final AppSession session) {
                if(session==null)return;
                Twitter.getApiClient(session).getSearchService().tweets(mCanal2Tag + " OR " + mMyCanal2Tag, null, null, null, RESULT_TYPE, count, null, null, finalMax_id ==0?null: finalMax_id, null, new Callback<Search>() {
                    @Override
                    public void success(Result<Search> result) {
                        //deliverResult((result.data.tweets));

                        newTweets((result.data.tweets));
                    }

                    @Override
                    public void failure(TwitterException e) {
                        //Toast.makeText(langdetect.MyApplication.context(), "could not load tweets", Toast.LENGTH_LONG).show();
                        final TwitterApiException apiException = (TwitterApiException) e;
                        final int errorCode = apiException.getErrorCode();
                        if (errorCode == TwitterApiConstants.Errors.APP_AUTH_ERROR_CODE || errorCode == TwitterApiConstants.Errors.GUEST_AUTH_ERROR_CODE) {
                            appSession = null;
                        }
                    }
                });
            }
        }.start(appSession);
        return null;
    }

    private List<Tweet> filterTweet(List<Tweet> tweets) {
        List<Tweet> filtered = new ArrayList<Tweet>();
        for (int i = 0; i < tweets.size(); i++) {
            Tweet tweet = tweets.get(i);
            try {
                ArrayList<Language> languages = LanguageChecker.getInstance(MyApplication.context()).detectLangs(tweet.text);
                if(languages==null||languages.size()==0)filtered.add(tweets.get(i));
                else{
                    if(languages.get(0).lang.equals("fr")||languages.get(0).lang.equals("en"))
                        filtered.add(tweets.get(i));
                }
            } catch (LangDetectException e) {
               // if(e.getCode()== ErrorCode.CantDetectError)
                {
                    filtered.add(tweets.get(i));
                }
                e.printStackTrace();
            }

        }
        TweetsLoadedEvent stickyEvent = EventBus.getDefault().getStickyEvent(TweetsLoadedEvent.class);

        if(stickyEvent!=null&&stickyEvent.getTweetList().size()<30)new TweetAsyncLoader().execute((long)HomeFragment.MAX_TWEET_ALLOWED,getTwitterMaxId(tweets));
        return filtered;
    }
    public long getTwitterMaxId(List<Tweet>list)
    {
        long max_id = Long.MAX_VALUE;
        for(Tweet t :list)
        {
            if(t.id<max_id)max_id = t.id;
        }
        return max_id;
    }
}
