package com.step4fun.mycanal2.canal2.model;

import android.view.View;
import android.widget.TextView;
import com.step4fun.mycanal2.canal2.R;

/**
 * Created by stephanekamga on 24/01/15.
 */
public class HeaderWrapper {
    public TextView mHeaderView;

    public HeaderWrapper(View baseView)
    {
        mHeaderView =(TextView)baseView.findViewById(R.id.shows_header);
    }

    public TextView getHeaderView() {
        return mHeaderView;
    }
}

