package com.step4fun.mycanal2.canal2.views;


import android.text.Editable;
import android.text.TextWatcher;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.step4fun.mycanal2.canal2.MainActivity;
import com.step4fun.mycanal2.canal2.MyApplication;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.controller.events.CommentsEvent;
import com.step4fun.mycanal2.canal2.controller.events.PostNewCommentException;
import com.step4fun.mycanal2.canal2.controller.networkservice.Service;

import com.step4fun.mycanal2.canal2.controller.twitterbootstrap.TweetAsyncLoader;
import com.step4fun.mycanal2.canal2.controller.twitterbootstrap.TwitterAction;
import com.step4fun.mycanal2.canal2.controller.twitterbootstrap.TwitterBootstrapUtils;
import com.step4fun.mycanal2.canal2.model.Comments;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.models.User;
import de.greenrobot.event.EventBus;
import org.joda.time.DateTime;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by stephanekamga on 21/02/15.
 */
public class TweetDialog extends MaterialDialog.ButtonCallback {
    @InjectView(R.id.tweet_profile)
    ImageView profile_pic;
    @InjectView(R.id.tweet_content)
    EditText content;
    @InjectView(R.id.tweet_username)
    TextView username;
    private MaterialDialog dialog;
    private static final String MAX_CHAR = "140";

    public TweetDialog init(MainActivity context) {
        dialog = new MaterialDialog.Builder(context).
                customView(R.layout.new_tweet, false)
                .title(MAX_CHAR)

                .titleColorRes(R.color.Blue_SteelBlue)
                .positiveText(R.string.tweet_it)
                .neutralText(R.string.tweet_cancel)
                .negativeText(R.string.post_locally)
                .neutralColorRes(R.color.Blue_SteelBlue)

                .titleGravity(GravityEnum.END)
                .backgroundColorRes(R.color.White_White)
                .dividerColorRes(R.color.Blue_SteelBlue)
                .positiveColorRes(R.color.Blue_SteelBlue)
                .negativeColorRes(R.color.Blue_SteelBlue)
                .cancelable(false)
                .callback(this)
                .build();

        ButterKnife.inject(this, dialog.getCustomView());

        loadProfilePic();
        setContentListener();

        if (Utils.getSizeName(context) < 2) {
            //small screen
            //we go full screen
            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setAttributes(params);
        } else {
        }

        return this;
    }

    public MaterialDialog show() {
        dialog.show();
        content.requestFocus();
        return dialog;
    }

    private void setContentListener() {
//        String tag = MyApplication.context().getString(R.string.mycanal2tag) + " " + MyApplication.context().getString(R.string.canal2tag);
//        content.setText(tag);
//        content.setTextColor(MyApplication.context().getResources().getColor(R.color.Gray));
        content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dialog.setTitle(String.valueOf(Integer.valueOf(MAX_CHAR) - s.length()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onNeutral(MaterialDialog dialog) {
        dialog.dismiss();
    }

    private String generateTweetText() {
        String tag = MyApplication.context().getString(R.string.mycanal2tag) + " " + MyApplication.context().getString(R.string.canal2tag);
        String text = tag + " " + content.getText().toString();

        return text;
    }

    @Override
    public void onPositive(final MaterialDialog dialog) {
        new TwitterAction() {
            @Override
            protected void runAction() {
//                TwitterBootstrapUtils.postTweet(content.getText().toString(), new Callback<Tweet>() {
                TwitterBootstrapUtils.postTweet(generateTweetText(), new Callback<Tweet>() {
                    //todo
                    @Override
                    public void success(Result<Tweet> result) {
                        dialog.dismiss();
                        new TweetAsyncLoader().newTweets(result.data);
                    }

                    @Override
                    public void failure(TwitterException e) {
                        dialog.dismiss();
                    }
                });
            }
        }.start();
    }

    /**
     * cf https://github.com/greenrobot/EventBus for how to get the event back and update the UI
     *
     * @param dialog
     */
    @Override
    public void onNegative(MaterialDialog dialog) {
        String tag1 = MyApplication.context().getString(R.string.mycanal2tag);
        String tag2 = MyApplication.context().getString(R.string.canal2tag);
        String text = content.getText().toString().replace(tag1, "").replace(tag2, "");
        final Comments c = new Comments(Utils.getPrimaryAccount(), text, "", DateTime.now().getMillis());
        CommentsEvent event = new CommentsEvent(c);
        EventBus.getDefault().post(event);
        Service.getInstance().addnewComment(c, new retrofit.Callback<Boolean>() {
            @Override
            public void success(Boolean success, Response response) {
                if (success) {
                    //Do nothing
                }
            }

            @Override
            public void failure(RetrofitError error) {
                // I guess we should remove the comment from the list
                PostNewCommentException exception = (PostNewCommentException) error.getCause();
                //TODO: Update the UI
                EventBus.getDefault().post(exception);
            }
        });
    }

    private void loadProfilePic() {
        if (TwitterBootstrapUtils.isUserLoggedIn()) {
            TwitterBootstrapUtils.getProfilePic(new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    if (!Utils.isNullOrEmpty(result.data.profileImageUrl)) {
                        Utils.load(MyApplication.context(), result.data.profileImageUrl, profile_pic);
                    }
                    if (!Utils.isNullOrEmpty(result.data.screenName)) {
                        username.setText("@" + result.data.screenName);
                    }
                }

                @Override
                public void failure(TwitterException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
