package com.step4fun.mycanal2.canal2.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.step4fun.mycanal2.canal2.MyApplication;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.controller.events.ProgramModelChangedEvent;
import com.step4fun.mycanal2.canal2.model.HeaderWrapper;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;
import com.step4fun.mycanal2.canal2.model.ShowItemViewWrapper;
import com.step4fun.mycanal2.canal2.model.UserShowPreference;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

import java.util.ArrayList;

/**
 * Created by stephanekamga on 24/01/15.
 */
public class ShowListViewAdapter extends BaseAdapter implements StickyListHeadersAdapter {
    private UserShowPreference preference;
    private ArrayList<ProgramsModel> source;

    public ShowListViewAdapter(ArrayList<ProgramsModel> objects) {
        preference = UserShowPreference.getInstance(Utils.getActivity());
        source = objects;
    }


    @Override
    public int getCount() {
        return source.size();
    }

    @Override
    public Object getItem(int i) {
        return source.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ProgramsModel current = source.get(position);
        ShowItemViewWrapper showWrapper = null;
        if (view == null) {
            LayoutInflater inflate = LayoutInflater.from(MyApplication.context());
            view = inflate.inflate(R.layout.shows_item, null);
            showWrapper = new ShowItemViewWrapper(view);
            view.setTag(showWrapper);
        } else {
            showWrapper = (ShowItemViewWrapper) view.getTag();
        }

        String imageUrl = current.getImageUrl();
        Utils.load(MyApplication.context(), imageUrl, showWrapper.mShowImageView);
        //Glide.with(getContext()).load(Uri.parse(imageUrl)).crossFade().into(showWrapper.mShowImageView);
//        showWrapper.mTime.setText(current.getStartTimeToString());
        showWrapper.mTitleTextView.setText(current.getTitle());
//        showWrapper.mDurationTextView.setText(current.getDurationToString());
        showWrapper.likeCount.setText(String.valueOf(current.getLikesCount()) + " " + MyApplication.context().getResources().getString(R.string.likes));
//        if (preference.isPreferredShow(current))
//            showWrapper.likeButton.setImageDrawable(getContext().getResources().getDrawable(R.drawable.like_red));
//        else showWrapper.likeButton.setImageDrawable(getContext().getResources().getDrawable(R.drawable.like_blue));
        showWrapper.viewCount.setText(String.valueOf(current.getViewsCount() + " " + MyApplication.context().getResources().getString(R.string.views)));
//        showWrapper.likeCount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int position = (Integer) view.getTag();
//                preference.switchPreference(getItem(position));
//                notifyDataSetChanged();
//
//            }
//        });
//        showWrapper.viewCount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                final int position = (Integer) view.getTag();
//                getItem(position).setViewCount(getItem(position).getViewCount()+1);
//                Service.getInstance().incrementViews(getItem(position), new Callback<Boolean>() {
//                    @Override
//                    public void success(Boolean aBoolean, Response response) {
//                        //
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        getItem(position).setViewCount(getItem(position).getViewCount()-1);
//                    }
//                });
//
//
//            }
//        });
        return view;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

//    @Override
//    public int getItemViewType(int position) {
//
//        return getItem(position).getType();
//    }

    private String parseSynopsisText(String synopsis) {
        if (synopsis.length() > 120) return synopsis.subSequence(0, 117).toString() + "...";
        else return synopsis;
    }

    @Override
    public View getHeaderView(int i, View convertView, ViewGroup viewGroup) {
        HeaderWrapper headerWrapper;
        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(MyApplication.context()).inflate(R.layout.shows_header, null);
            headerWrapper = new HeaderWrapper(v);
            v.setTag(headerWrapper);
        } else {
            headerWrapper = (HeaderWrapper) v.getTag();
        }
        headerWrapper.getHeaderView().setText(source.get(i).getSection().toUpperCase());

        return v;
    }

    @Override
    public long getHeaderId(int i) {
        return source.get(i).getSection().toUpperCase().hashCode();
    }

    public void onEventMainThread(ProgramModelChangedEvent event) {
        ProgramsModel result = event.getUpdate();
        int i = 0;
        for (int j = 0; j < source.size(); j++) {
            if (source.get(j).getId() == result.getId()) {
                i = j;
                break;
            }
        }
        source.remove(i);
        source.add(i, result);
        notifyDataSetChanged();
    }

    public void setItem(ArrayList<ProgramsModel> list) {
        source.clear();
        source.addAll(list);
        notifyDataSetChanged();
    }
}
