package com.step4fun.mycanal2.canal2.views.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bluejamesbond.text.DocumentView;
import com.step4fun.mycanal2.canal2.MainActivity;
import com.step4fun.mycanal2.canal2.R;

/**
 * Created by stephanekamga on 21/01/15.
 */
public class AboutFragment extends Fragment implements View.OnClickListener {
    private TextView website;
    private TextView cana2Site;
    private TextView twitterSite;
    private TextView facebookSite;
    private ImageView left;
    private ImageView right;
    private LinearLayout descriptionBlock;
    private DocumentView descriptionJust;
    private TextView quoteAuthor;
    private ImageButton pencil;
    private TextView hide;
    private int mode = 0;
    private Animation mWiggle;
    Typeface contentFont;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(MainActivity.CURRENT_FRAGMENT_KEY, MainActivity.AboutFragmentTag);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.about_author, container, false);
        contentFont=Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/OldStandard_Regular.ttf");
        left = (ImageView) v.findViewById(R.id.profile_image_left);
        right = (ImageView) v.findViewById(R.id.profile_image_right);
        descriptionBlock = (LinearLayout) v.findViewById(R.id.about_author_linearlayout2_id);
        descriptionJust = (DocumentView) v.findViewById(R.id.about_author_description_just);
        quoteAuthor = (TextView) v.findViewById(R.id.about_author_quote_author);
        pencil = (ImageButton) v.findViewById(R.id.about_author_write);
        hide = (TextView) v.findViewById(R.id.about_author_hide);
        right.setOnClickListener(this);
        left.setOnClickListener(this);
        hide.setOnClickListener(this);
        pencil.setOnClickListener(this);

        website = (TextView) v.findViewById(R.id.website_link);
        website.setOnClickListener(this);
        website.setMovementMethod(LinkMovementMethod.getInstance());

        cana2Site = (TextView) v.findViewById(R.id.canaltwo_link);
        cana2Site.setOnClickListener(this);
        cana2Site.setMovementMethod(LinkMovementMethod.getInstance());

        twitterSite = (TextView) v.findViewById(R.id.twitter_link);
        twitterSite.setOnClickListener(this);
        twitterSite.setMovementMethod(LinkMovementMethod.getInstance());

        facebookSite = (TextView) v.findViewById(R.id.facebook_link);
        facebookSite.setOnClickListener(this);
        facebookSite.setMovementMethod(LinkMovementMethod.getInstance());

        return v;
    }

    public void animate(View view) {
        if (mWiggle == null) {
            mWiggle = AnimationUtils.loadAnimation(getActivity(), R.anim.wiggle);
        }
        view.startAnimation(mWiggle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.website_link: {
                String url = null;
                if (mode == 1) url = "";
                else url = "";
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
                break;
            }
            case R.id.twitter_link: {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.twitter_link)));
                startActivity(browserIntent);
                break;
            }
            case R.id.facebook_link: {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.facebook_link)));
                startActivity(browserIntent);
                break;
            }
            case R.id.canaltwo_link: {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.canal2_link)));
                startActivity(browserIntent);
                break;
            }
            case R.id.profile_image_left: {
                animate(v);
                mode = -1;
                descriptionBlock.setVisibility(View.VISIBLE);
                descriptionJust.setText(getResources().getString(R.string.description_author_left));
//                quoteAuthor.setTypeface(contentFont);
                quoteAuthor.setText(getResources().getString(R.string.author_quote_left));
                website.setVisibility(View.VISIBLE);
                twitterSite.setVisibility(View.VISIBLE);
                facebookSite.setVisibility(View.GONE);
                break;
            }
            case R.id.profile_image_right: {
                descriptionBlock.setVisibility(View.VISIBLE);
                descriptionJust.setText(getResources().getString(R.string.description_author_right));
//                quoteAuthor.setTypeface(contentFont);
                quoteAuthor.setText(getResources().getString(R.string.author_quote_right));
                animate(v);
                twitterSite.setVisibility(View.GONE);
                facebookSite.setVisibility(View.VISIBLE);
                website.setVisibility(View.VISIBLE);
                mode = 1;
                break;
            }
            case R.id.about_author_hide: {
                descriptionBlock.setVisibility(View.GONE);
                break;
            }
            case R.id.about_author_write: {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.contact_email)});
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                break;
            }
        }
    }
}
