package com.step4fun.mycanal2.canal2.controller.networkservice;

import com.step4fun.mycanal2.canal2.model.ProgramsModel;
import retrofit.Callback;
import retrofit.http.*;

import java.util.List;

/**
 * Created by stephanekamga on 16/03/15.
 */
public interface ShowsApi {

        @GET("/shows")
        public void listShows(Callback<List<ProgramsModel>>cb);

        @POST("/shows/{show_id}/views/")
        public void incrementViewCount(@Path("show_id") long id, Callback<ProgramsModel> vb);

        @POST("/shows/")
        public void updateShow(@Body ProgramsModel model, Callback<ProgramsModel> vb);

        @POST("/shows/{show_id}/like")
        public void incrementLikeCount(@Path("show_id") long id, @Query("value") int count, Callback<ProgramsModel> vb);

}
