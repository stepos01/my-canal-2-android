package com.step4fun.mycanal2.canal2.controller.alarm;

import android.os.Build;
import com.step4fun.mycanal2.canal2.MainActivity;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;


import java.util.List;

/**
 * Created by stephanekamga on 22/01/15.
 */
public abstract class ReminderService {

    private ReminderSaver mSaver;
    protected MainActivity mContext;
    private ReminderServiceNotification reminderServiceNotification;
    public ReminderService(MainActivity context)
    {
        mSaver = ReminderSaver.getInstance(context);
        mContext = context;
    }
    public static boolean canUseCalendarICS() {
        return Build.VERSION.SDK_INT >= 14;
    }
    public  boolean hasReminder(ProgramsModel model) {
        return hasReminder(model.getId());
    }
    public  boolean hasReminder(long id) {
        return mSaver.hasReminder(id);
    }
    public abstract void addReminder(ProgramsModel model);
    protected void save(ProgramsModel model)
    {
        mSaver.setReminder(model);
        if(reminderServiceNotification!=null)reminderServiceNotification.notifySender(true);

    }
    public void removeReminder(ProgramsModel model)
    {
         mSaver.removeReminder(model);
        if(reminderServiceNotification!=null)reminderServiceNotification.notifySender(true);
    }
    protected List<ProgramsModel> getReminderList()
    {
        return mSaver.getReminderList();
    }




    protected void getRemindedProgram(ProgramsModel model)
    {
        mSaver.getRemindedProgram(model.getId());
    }

    public void setReminderServiceNotification(ReminderServiceNotification reminderServiceNotification) {
        this.reminderServiceNotification = reminderServiceNotification;
    }

    public interface ReminderServiceNotification
    {
        void notifySender(boolean isSuccess);
    }
}
