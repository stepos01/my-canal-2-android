package com.step4fun.mycanal2.canal2.controller.events;

import com.step4fun.mycanal2.canal2.model.AppInfo;

/**
 * Created by sngatchou on 31/03/2015.
 */
public class AppInfoEvent implements EventStub {
    private final AppInfo appInfo;

    public AppInfoEvent(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    public AppInfo getAppInfo() {
        return appInfo;
    }
}
