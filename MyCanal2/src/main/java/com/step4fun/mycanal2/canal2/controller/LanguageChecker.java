package com.step4fun.mycanal2.canal2.controller;



import android.content.Context;
import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import com.cybozu.labs.langdetect.Language;
import com.step4fun.mycanal2.canal2.MyApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by stephanekamga on 24/03/15.
 */
public class LanguageChecker {

    private static LanguageChecker instance;

    private LanguageChecker(Context c) throws IOException, LangDetectException {
        init(c);
    }

    public static LanguageChecker getInstance(Context c)
    {

        build(c);
        return instance;
    }
    private   static void build(Context context)
    {
        if(instance == null) try {
            instance = new LanguageChecker(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LangDetectException e) {
            e.printStackTrace();
        }


    }


    public void init(Context c) throws LangDetectException {
        try {
            String[] list = c.getAssets().list("");
            ArrayList<String>json = new ArrayList<String>();
            for (String l:list)
            {
                if(l.length()==2){
                    InputStream open = MyApplication.context().getAssets().open(l);
                    BufferedReader r = new BufferedReader(new InputStreamReader(open));
                    StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line);
                    }
                    json.add(total.toString());
                }

            }
            DetectorFactory.loadProfile(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String detect(String text) throws LangDetectException {
        Detector detector = DetectorFactory.create();
        detector.append(text);
        return detector.detect();
    }
    public ArrayList<Language> detectLangs(String text) throws LangDetectException {
        Detector detector = DetectorFactory.create();
        detector.append(text);
        return detector.getProbabilities();
    }

}
