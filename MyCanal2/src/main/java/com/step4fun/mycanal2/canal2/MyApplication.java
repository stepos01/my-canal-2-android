package com.step4fun.mycanal2.canal2;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import net.danlew.android.joda.JodaTimeAndroid;

public class MyApplication extends Application {
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "Z8UYyaf0TZOIhTAUZ1vVGIsFP";
    private static final String TWITTER_SECRET = "mJ0g2CF9gjGmlNRot7ScAopcKymepISFkY35mjx8ThJGmdiRo4";
    private static Context context;

    /*
     * (non-Javadoc)
     *
     * @see android.app.Application#onCreate()
     */
    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Crashlytics crashlytics = new Crashlytics.Builder().disabled(BuildConfig.DEBUG).build();

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Twitter(authConfig), new Crashlytics())
                .debuggable(true)
                .build();

        Fabric.with(fabric);
        MyApplication.context = getApplicationContext();
        JodaTimeAndroid.init(this);
    }

    /**
     * This method gives you the application context
     *
     * @return
     */
    public static Context context() {
        return MyApplication.context;
    }

    private Activity mCurrentActivity = null;

    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(Activity pCurrentActivity) {
        mCurrentActivity = pCurrentActivity;
    }
}
