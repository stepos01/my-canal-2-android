package com.step4fun.mycanal2.canal2.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by stephanekamga on 17/01/15.
 */
public class ProgramsModel implements Cloneable,Parcelable{
    //todo extends dataSavedWrapper and implements parcelable
    private String synopsis;
    private long startTime;
    private long endTime;
    private String title;
    private long id;
    private String imageUrl;
    private long reminderId;
    private boolean isReminderSet;
    private int type;
    private String section;
    private String videoId;
    private boolean isPlaylist;
    private ArrayList<ShowsVideo> showsVideos;
    private int viewsCount;
    private int likesCount;
    /**
     * frequency of the event in days
     */
    private int frequency;

    @Override
    public void writeToParcel(Parcel parcel, int i) {
      parcel.writeString(synopsis);
        parcel.writeLong(startTime);
        parcel.writeLong(endTime);
        parcel.writeString(title);
        parcel.writeLong(id);
        parcel.writeString(imageUrl);
        parcel.writeLong(reminderId);
        parcel.writeInt(isReminderSet?1:-1);
        parcel.writeInt(type);
        parcel.writeString(section);
        parcel.writeString(videoId);
        parcel.writeInt(isPlaylist?1:-1);
        parcel.writeTypedList(showsVideos);
        parcel.writeInt(viewsCount);
        parcel.writeInt(likesCount);

    }
    public static final Creator<ProgramsModel> CREATOR
            = new Creator<ProgramsModel>() {
        public ProgramsModel createFromParcel(Parcel in) {
            return new ProgramsModel(in);
        }

        public ProgramsModel[] newArray(int size) {
            return new ProgramsModel[size];
        }
    };

    public ProgramsModel(Parcel in) {
        synopsis = in.readString();
        startTime = in.readLong();
        endTime = in.readLong();
        title = in.readString();
        id = in.readLong();
        imageUrl =in.readString();
        reminderId = in.readLong();
        isReminderSet = in.readInt() == 1;
        type = in.readInt();
        section = in.readString();
        videoId = in.readString();
        isPlaylist=in.readInt()==1;
        showsVideos = new ArrayList<>();
        in.readTypedList(showsVideos, ShowsVideo.CREATOR);
        viewsCount = in.readInt();
        likesCount = in.readInt();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ProgramsModel o = (ProgramsModel) super.clone();
        if (showsVideos != null) {
            ArrayList<ShowsVideo> list = new ArrayList<>();
            for (ShowsVideo video : showsVideos) {
                list.add((ShowsVideo) video.clone());
            }
            o.setShowsVideos(list);
        }
        return o;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isReminderSet() {
        return isReminderSet;
    }

    public void setReminderSet(boolean isReminderSet) {
        this.isReminderSet = isReminderSet;
    }

    public long getReminderId() {
        return reminderId;
    }

    public void setReminderId(long reminderId) {
        this.reminderId = reminderId;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public boolean isPlaylist() {
        return isPlaylist;
    }

    public void setPlaylist(boolean isPlaylist) {
        this.isPlaylist = isPlaylist;
    }

    public ArrayList<ShowsVideo> getShowsVideos() {
        return showsVideos;
    }

    public void setShowsVideos(ArrayList<ShowsVideo> showsVideos) {
        this.showsVideos = showsVideos;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }




    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ProgramsModel() {
    }
    public ProgramsModel(String section, int type)
    {
        this.section = section;
        this.type = type;
    }








     /*
                ANDROID RELATED
      */




//    public ShowsDetailsModel getDetails() {
//        ShowsDetailsModel model = new ShowsDetailsModel();
//        model.setDescription(getSynopsis());
//        model.setTime(getStartTimeToString());
//        model.setTitle(getTitle());
//        ArrayList<String> list = new ArrayList<>();
//        list.add(getImageUrl());
//        list.add(getImageUrl());
//        model.setImageUrl(list);
//        model.setDuration(getDurationToString());
//        model.setSection(getSection());
//        model.setVideoId("cdgQpa1pUUE");
//        model.setIsPlayList(false);
//        return model;
//    }
//    public String getDurationToString()
//    {
//        if(!isTimeSet())return "";
//        Period p = new Period(startTime,endTime, PeriodType.dayTime());
//        LocalTime localTime = new LocalTime(p.getHours(),p.getMinutes());
//        return  localTime.toString("h'h'mm'mn'");
//    }
//    public boolean isToday()
//    {
//        if(!isTimeSet())return false;
//        DateTime s = new DateTime(startTime);
//        int day =  s.dayOfWeek().get();
//        int hour = s.hourOfDay().get();
//        int minute = s.minuteOfHour().get();
//        DateTime showdate = new DateTime()
//                .withDayOfWeek(day)
//                .withHourOfDay(hour)
//                .withMinuteOfHour(minute);
//        return DateUtils.isToday(showdate);
//    }
//    public String getStartTimeToString()
//    {
//        return convertTime2PrettyString(startTime);
//    }

//    public String getEndTimeToString()
//    {
//        return convertTime2PrettyString(endTime);
//    }
//
//    private String convertTime2PrettyString(long time)
//    {
//        if(!isTimeSet())return "";
//        DateTime s = new DateTime(time);
//        int day =  s.dayOfWeek().get();
//        int hour = s.hourOfDay().get();
//        int minute = s.minuteOfHour().get();
//        DateTime showdate = new DateTime()
//                .withDayOfWeek(day)
//                .withHourOfDay(hour)
//                .withMinuteOfHour(minute);
//        if(DateUtils.isToday(showdate))
//        {
//            //it's today
//            //we use relative time
//            return  DateUtils.getRelativeTimeSpanString(langdetect.MyApplication.context(), showdate).toString();
//        }
//        else {
//            if(frequency==1)
//            {
//                //every day show
//                return "7/7 "+ showdate.toString("H'h'mm ", Locale.getDefault());
//            }else
//            {
//                //weekly
//                return showdate.toString("EEEE, H'h'mm ", Locale.getDefault());
//            }
//
//        }
//
//    }


    public boolean isTimeSet() {
        if(frequency==0)return false;
        if(startTime==0)return false;
        if(endTime==0)return false;
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }


}
