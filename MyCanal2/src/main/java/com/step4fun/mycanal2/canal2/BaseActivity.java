package com.step4fun.mycanal2.canal2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by stephanekamga on 18/03/15.
 */
public class BaseActivity extends ActionBarActivity{
        protected MyApplication mMyApp;

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mMyApp = (MyApplication)this.getApplicationContext();
        }
        protected void onResume() {
            super.onResume();
            mMyApp.setCurrentActivity(this);
        }
        protected void onPause() {
            clearReferences();
            super.onPause();
        }
        protected void onDestroy() {
            clearReferences();
            super.onDestroy();
        }

        private void clearReferences(){
            Activity currActivity = mMyApp.getCurrentActivity();
            if (currActivity != null && currActivity.equals(this))
                mMyApp.setCurrentActivity(null);
        }
    }

