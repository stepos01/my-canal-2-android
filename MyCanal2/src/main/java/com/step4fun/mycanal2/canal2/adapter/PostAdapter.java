package com.step4fun.mycanal2.canal2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.step4fun.mycanal2.canal2.views.specific.Item;
import com.step4fun.mycanal2.canal2.views.specific.RowType;

import java.util.List;

/**
 * Created by sngatchou on 19/03/2015.
 */
public class PostAdapter<T extends Item> extends BaseAdapter {
    private List<T> elements;

    private Context mContext;

    public PostAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public PostAdapter(Context mContext, List<T> elements) {
        this.mContext = mContext;
        this.elements = elements;
    }

    public void setElements(List<T> elements) {
        this.elements = elements;
        notifyDataSetChanged();
    }

    private void addElements(List<T> elements) {
        this.elements.addAll(elements);
        notifyDataSetChanged();
    }

    public void addNewItems(List<T> elements) {
        addElements(elements);
    }

    public void swapItems(List<T> elements) {
        // Add elements in the 3rd position because
        // first position is filled by the advertising
        // and the second one is filled by the header
        this.elements.addAll(2, elements);
        notifyDataSetChanged();
    }

    public void errorItems(List<T> elements) {
        // An error happens then, we clear the list
        // And rebuild a new one according to the error
        this.elements.clear();
        this.addElements(elements);
    }

    public void noCommentsItems(List<T> elements) {
        // There is no comment in the database
        this.elements.clear();
        this.addElements(elements);
    }

    public void removeItems(int position) {
        this.elements.remove(position);
        this.notifyDataSetChanged();
    }

    public List<T> getElements() {
        return elements;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return elements.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return elements.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return elements.get(position).getItemViewType();
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this.mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        return elements.get(position).getView(inflater, position, convertView, parent);
    }

    @Override
    public int getViewTypeCount() {
        return RowType.values().length;
    }
}

