package com.step4fun.mycanal2.canal2.controller.alarm;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import com.step4fun.mycanal2.canal2.MainActivity;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;

import java.util.TimeZone;

/**
 * Created by stephanekamga on 21/01/15.
 */
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class CalendarService extends ReminderService implements CalendarQueryHandler.OnCalendarQueryCompletedListener {

    public static final String PROGRAMS_TITLE = "PROGRAMS_TITLE";
    public static final String PROGRAMS_TEXT = "PROGRAMS_TEXT";
    private ProgramsModel mModel;
    public static  String[] EVENT_PROJECTION = new String[0];
    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
    private static final int PROJECTION_DISPLAY_NAME_INDEX = 2;
    private static final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;

    String MY_ACCOUNT_NAME = "MY_CANAL_2_CALENDAR_ACCOUNT";
    String CALENDAR_NAME = "MY_CANAL_2_CALENDAR_NAME";
    String CALENDAR_DISPLAY_NAME = "MY CANAL 2 CALENDAR";

    public CalendarService(MainActivity context) {
        super(context);

        }



    @Override
    public void addReminder(ProgramsModel model) {
        mModel = model;
        getCalendarID();

    }

    private void getCalendarID() {
        EVENT_PROJECTION = new String[] {
                CalendarContract.Calendars._ID,                           // 0
                CalendarContract.Calendars.ACCOUNT_NAME,                  // 1
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,         // 2
                CalendarContract.Calendars.OWNER_ACCOUNT                  // 3
        };
        // Run query
        Cursor cur = null;
        ContentResolver cr = mContext.getContentResolver();
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String selection =
                 CalendarContract.Calendars.ACCOUNT_TYPE + " = ? AND "
                +CalendarContract.Calendars.OWNER_ACCOUNT + " = ? ";
        String[] selectionArgs = new String[] {
                CalendarContract.ACCOUNT_TYPE_LOCAL,
                MY_ACCOUNT_NAME
                };
        // Submit the query and get a Cursor object back.
        CalendarQueryHandler mCalenderQueryHandler = new CalendarQueryHandler(cr);
        mCalenderQueryHandler.setListener(this);
        mCalenderQueryHandler.startQuery(CalendarQueryHandler.LOAD_CALENDAR_TOKEN_ID, null, uri, EVENT_PROJECTION, selection, selectionArgs, null);
    }

    @Override
    public void removeReminder(ProgramsModel model) {
        mModel = model;
        ContentResolver cr = mContext.getContentResolver();
        Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, model.getReminderId());
//        int rows = cr.delete(deleteUri, null, null);
        CalendarQueryHandler mCalenderQueryHandler = new CalendarQueryHandler(cr);
        mCalenderQueryHandler.startDelete(CalendarQueryHandler.REMOVE_EVENT_TOKEN,null,deleteUri,null,null);
        mCalenderQueryHandler.setListener(this);
    }


    private void addReminder2Calendar(long eventID)
    {
        ContentResolver cr = mContext.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Reminders.MINUTES, 30);
        values.put(CalendarContract.Reminders.EVENT_ID, eventID);
        values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        CalendarQueryHandler  mCalenderQueryHandler = new CalendarQueryHandler(cr);
        mCalenderQueryHandler.setListener(this);
        mCalenderQueryHandler.startInsert(CalendarQueryHandler.ADD_REMINDER,null,CalendarContract.Reminders.CONTENT_URI,values);
    }

    private void insertInCalendar(long calID)
    {
        long startMillis = mModel.getStartTime();
        long endMillis = mModel.getEndTime();
        ContentResolver cr = mContext.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.RDATE,"FREQ=WEEKLY;WKST=SU");
        values.put(CalendarContract.Events.TITLE, mContext.getResources().getString(R
                .string.app_name));
        values.put(CalendarContract.Events.DESCRIPTION, mModel.getTitle());
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getDisplayName());

        CalendarQueryHandler mCalenderQueryHandler = new CalendarQueryHandler(cr);
        mCalenderQueryHandler.setListener(this);
        mCalenderQueryHandler.startInsert(CalendarQueryHandler.CREATE_EVENT_TOKEN_ID, null, CalendarContract.Events.CONTENT_URI, values);

    }
    @Override
    public void OnLoadCalendarId(int token, Object cookie, Cursor cursor) {
        long calID = -1;
        if(cursor==null)
        {
            createLocalCalendar();
            return;
        }
        if (cursor.moveToNext()) {
            String displayName = null;
            String accountName = null;
            String ownerName = null;

            // Get the field values
            calID = cursor.getLong(PROJECTION_ID_INDEX);
            displayName = cursor.getString(PROJECTION_DISPLAY_NAME_INDEX);
            accountName = cursor.getString(PROJECTION_ACCOUNT_NAME_INDEX);
            ownerName = cursor.getString(PROJECTION_OWNER_ACCOUNT_INDEX);
            }
        if(calID!=-1)
        {
              insertInCalendar(calID);
        }
    }

    @Override
    public void OnEventCreated(int token, Object cookie, Uri uri) {
        if(uri.toString().contains(CalendarContract.Events.CONTENT_URI.toString())) {
            long eventId = Long.parseLong(uri.getLastPathSegment());
            mModel.setReminderId(eventId);
            addReminder2Calendar(eventId);
        }

    }
    private void createLocalCalendar()
    {


        ContentValues values = new ContentValues();
        values.put(
                CalendarContract.Calendars.ACCOUNT_NAME,
                MY_ACCOUNT_NAME);
        values.put(
                CalendarContract.Calendars.ACCOUNT_TYPE,
                CalendarContract.ACCOUNT_TYPE_LOCAL);
        values.put(
                CalendarContract.Calendars.NAME,
                CALENDAR_NAME);
        values.put(
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                CALENDAR_DISPLAY_NAME);
        values.put(
                CalendarContract.Calendars.CALENDAR_COLOR,
                0xffff0000);
        values.put(
                CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL,
                CalendarContract.Calendars.CAL_ACCESS_OWNER);
        values.put(
                CalendarContract.Calendars.OWNER_ACCOUNT,
                MY_ACCOUNT_NAME);
        values.put(
                CalendarContract.Calendars.CALENDAR_TIME_ZONE,
                TimeZone.getDefault().getDisplayName());
        values.put(
                CalendarContract.Calendars.SYNC_EVENTS,
                1);
        Uri.Builder builder =
                CalendarContract.Calendars.CONTENT_URI.buildUpon();
        builder.appendQueryParameter(
                CalendarContract.Calendars.ACCOUNT_NAME,
                MY_ACCOUNT_NAME);
        builder.appendQueryParameter(
                CalendarContract.Calendars.ACCOUNT_TYPE,
                CalendarContract.ACCOUNT_TYPE_LOCAL);
        builder.appendQueryParameter(
                CalendarContract.CALLER_IS_SYNCADAPTER,
                "true");
//        Uri uri =
//                mContext.getContentResolver().insert(builder.build(), values);
        CalendarQueryHandler mCalenderQueryHandler = new CalendarQueryHandler(mContext.getContentResolver());
        mCalenderQueryHandler.setListener(this);
        mCalenderQueryHandler.startInsert(CalendarQueryHandler.CREATE_CALENDAR_ID, null, builder.build(), values);

    }
    @Override
    public void OnReminderCreated(int token, Object cookie, Uri uri) {
        super.save(mModel);
    }

    @Override
    public void OnDeleteEvent(int token, Object cookie, int result) {
        super.removeReminder(mModel);
    }

    @Override
    public void OnCalendarCreated(int token, Object cookie, Uri uri) {
        getCalendarID();
    }
}

