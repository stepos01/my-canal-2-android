package com.step4fun.mycanal2.canal2.views.specific.impl;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.step4fun.mycanal2.canal2.MyApplication;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.model.Comments;
import com.step4fun.mycanal2.canal2.views.specific.Item;
import com.step4fun.mycanal2.canal2.views.specific.RowType;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;
import java.util.Locale;

/**
 * Created by sngatchou on 19/03/2015.
 */
public class RowItem<T extends Comments> extends Item {
    private T element;

    public RowItem(T element) {
        this.element = element;
    }

    public T getElement() {
        return element;
    }

    @Override
    public View getView(LayoutInflater inflater, int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.home_list_contents, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        displayView(holder);

        return convertView;
    }

    private void displayView(ViewHolder holder) {
        holder.username.setText(Utils.buildUsername(Utils.split(element.getUsername())));
        holder.comment.setText(element.getComment());
        holder.dateTime.setText(Utils.niceDate(element.getDateTime(), buildFormat().toString()));
    }

    private StringBuilder buildFormat() {
        final StringBuilder format = new StringBuilder();
        final String fr = MyApplication.context().getResources().getString(R.string.iso_country_fr);
        final String it = MyApplication.context().getResources().getString(R.string.iso_country_it);
        final String country = Utils.getUserLocale().getCountry();
        format.append("E dd MMM yyyy '").append(MyApplication.context().getResources().getString(R.string.at));
        if (country.equals(fr)) {
            format.append("' HH'").append(MyApplication.context().getResources().getString(R.string.hour))
                    .append("'mm");
        } else if (country.equals(it)) {
            format.append("' HH:mm");
        }else {
            format.append("' HH.mm a");
        }

        return format;
    }

    @Override
    public int getItemViewType() {
        return RowType.ROW.ordinal();
    }

    static class ViewHolder {
        @InjectView(R.id.home_list_contents_profile_img_id)
        CircleImageView profImage;
        @InjectView(R.id.home_list_contents_username)
        TextView username;
        @InjectView(R.id.home_list_contents_comment_id)
        TextView comment;
        @InjectView(R.id.home_list_contents_app_date_comment_id)
        TextView dateTime;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
