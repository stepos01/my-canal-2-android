package com.step4fun.mycanal2.canal2.model;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.step4fun.mycanal2.canal2.R;

/**
 * Created by stephanekamga on 24/01/15.
 */
public class ShowItemViewWrapper  {

    @InjectView(R.id.show_item_title)
    public TextView mTitleTextView;
    @InjectView(R.id.shows_item_view_count_id)
    public TextView viewCount;
    @InjectView(R.id.shows_item_like_count_id)
    public TextView likeCount;
    @InjectView(R.id.show_item_image)public ImageView mShowImageView;
//    @InjectView(R.id.show_item_duration)public   TextView mDurationTextView;

    public ShowItemViewWrapper(View view)
    {
        ButterKnife.inject(this,view);
    }

//    public static void switchFavorite(Context context, ImageView v, boolean isFavorite) {
//        if (isFavorite) v.setImageDrawable(context.getResources().getDrawable(R.drawable.reminders));
//        else v.setImageDrawable(context.getResources().getDrawable(R.drawable.no_reminders));
//    }
}
