package com.step4fun.mycanal2.canal2.adapter;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by sngatchou on 24/03/2015.
 *
 * From https://github.com/github/android/blob/master/app/src/main/java/com/github/mobile/ui/HeaderFooterListAdapter.java
 */
public class HeaderFooterListAdapter<E extends BaseAdapter> extends HeaderViewListAdapter {
    private final ArrayList<ListView.FixedViewInfo> headers;

    private final ArrayList<ListView.FixedViewInfo> footers;

    private final ListView listView;

    private final E wrapped;

    /**
     * Create header footer adapter
     *
     * @param listView
     * @param adapter
     */
    public HeaderFooterListAdapter(ListView listView, E adapter) {
        this(new ArrayList<ListView.FixedViewInfo>(), new ArrayList<ListView.FixedViewInfo>(), listView, adapter);
    }

    private HeaderFooterListAdapter(ArrayList<ListView.FixedViewInfo> headers, ArrayList<ListView.FixedViewInfo> footers, ListView listView, E adapter) {
        super(headers, footers, adapter);
        this.headers = headers;
        this.footers = footers;
        this.listView = listView;
        this.wrapped = adapter;
    }

    /**
     * Add non-selectable header view with no data
     *
     * @param view
     * @return this adapter
     * @see #addHeader(android.view.View, Object, boolean)
     */
    public HeaderFooterListAdapter<E> addHeader(View view) {
        return addHeader(view, null, false);
    }

    /**
     * Add header
     *
     * @param view
     * @param data
     * @param isSelectable
     * @return this adapter
     */
    public HeaderFooterListAdapter<E> addHeader(View view, Object data, boolean isSelectable) {
        ListView.FixedViewInfo info = this.listView.new FixedViewInfo();

        /* Initialize FixedViewInfo attributes */
        info.data = data;
        info.isSelectable = isSelectable;
        info.view = view;

        /* Add header */
        this.headers.add(info);

        /* Notify the change to the wrapped adapter */
        this.wrapped.notifyDataSetChanged();

        return this;
    }

    /**
     * Add non-selectable header view with no data
     *
     * @param view
     * @return this adapter
     * @see #addHeader(View, Object, boolean)
     */
    public HeaderFooterListAdapter<E> addFooter(View view) {
        return addFooter(view, null, false);
    }

    /**
     * Add header
     *
     * @param view
     * @param data
     * @param isSelectable
     * @return this adapter
     */
    public HeaderFooterListAdapter<E> addFooter(View view, Object data, boolean isSelectable) {
        ListView.FixedViewInfo info = this.listView.new FixedViewInfo();

        /* Initialize FixedViewInfo attributes */
        info.data = data;
        info.isSelectable = isSelectable;
        info.view = view;

        /* Add footer */
        this.footers.add(info);

        /* Notify the change to the wrapped adapter */
        this.wrapped.notifyDataSetChanged();

        return this;
    }

    @Override
    public boolean removeHeader(View v) {
        boolean removed = super.removeHeader(v);

        if (removed) {
            this.wrapped.notifyDataSetChanged();
        }

        return removed;
    }

    @Override
    public boolean removeFooter(View v) {
        boolean removed = super.removeFooter(v);

        if (removed) {
            this.wrapped.notifyDataSetChanged();
        }

        return removed;
    }

    /**
     * Remove all headers
     *
     * @return true if headers were removed, false otherwise
     */
    public boolean clearHeaders() {
        boolean removed = false;

        if (!this.headers.isEmpty()) {
            ListView.FixedViewInfo[] infos = this.headers.toArray(
                    new ListView.FixedViewInfo[this.headers.size()]);

            for (ListView.FixedViewInfo info : infos) {
                removed = super.removeHeader(info.view) || removed;
            }
        }

        if (removed) {
            this.wrapped.notifyDataSetChanged();
        }

        return removed;
    }

    /**
     * Remove all footers
     *
     * @return true if headers were removed, false otherwise
     */
    public boolean clearFooters() {
        boolean removed = false;

        if (!this.footers.isEmpty()) {
            ListView.FixedViewInfo[] infos = this.footers.toArray(
                    new ListView.FixedViewInfo[this.footers.size()]);

            for (ListView.FixedViewInfo info : infos) {
                removed = removed || super.removeFooter(info.view);
            }

            if (removed) {
                this.wrapped.notifyDataSetChanged();
            }
        }

        return removed;
    }

    @Override
    public E getWrappedAdapter() {
        return this.wrapped;
    }

    @Override
    public boolean isEmpty() {
        return this.wrapped.isEmpty();
    }

    public View getFooterView(int index) {
        return this.footers.get(index).view;
    }
}
