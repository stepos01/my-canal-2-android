package com.step4fun.mycanal2.canal2.controller.events;

import com.step4fun.mycanal2.canal2.model.Comments;

/**
 * Created by sngatchou on 21/03/2015.
 */
public class CommentsEvent implements EventStub {
    private Comments comment;

    public CommentsEvent(Comments comment) {
        this.comment = comment;
    }

    public Comments getComment() {
        return comment;
    }
}
