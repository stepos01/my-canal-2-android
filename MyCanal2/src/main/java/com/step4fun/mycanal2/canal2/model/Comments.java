package com.step4fun.mycanal2.canal2.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.TimeZone;

public class Comments implements Parcelable {
    private String username;

    private String comment;

    private String thumbUrl;

    private long dateTime;

    public Comments() {
    }

    public Comments(String username, String comment, String thumbUrl, long dateTime) {
        this.username = username;
        this.comment = comment;
        this.thumbUrl = thumbUrl;
        setDateTime(dateTime);
    }

    public Comments(Parcel source) {
        username = source.readString();
        comment = source.readString();
        thumbUrl = source.readString();
        dateTime = source.readLong();
    }

    public static final Creator<Comments> CREATOR
            = new Creator<Comments>() {
        public Comments createFromParcel(Parcel in) {
            return new Comments(in);
        }

        public Comments[] newArray(int size) {
            return new Comments[size];
        }
    };

    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the set of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(comment);
        dest.writeString(thumbUrl);
        dest.writeLong(dateTime);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = new DateTime(dateTime).toDateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Africa/Douala"))).getMillis();
    }

    public long getCurrentTimeZonedDateTime() {
        return new DateTime(dateTime).toDateTime(DateTimeZone.getDefault()).getMillis();
    }
}
