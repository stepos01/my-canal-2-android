package com.step4fun.mycanal2.canal2.views.specific.impl;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.views.specific.Item;
import com.step4fun.mycanal2.canal2.views.specific.RowType;

/**
 * Created by sngatchou on 20/03/2015.
 */
public class NoCommentsItem extends Item {
    @Override
    public View getView(LayoutInflater inflater, int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.home_list_no_comments, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        return convertView;
    }

    @Override
    public int getItemViewType() {
        return RowType.NO_COMMENTS.ordinal();
    }

    static class ViewHolder {
        @InjectView(R.id.home_list_empty_no_comments_id)
        TextView noComments;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
