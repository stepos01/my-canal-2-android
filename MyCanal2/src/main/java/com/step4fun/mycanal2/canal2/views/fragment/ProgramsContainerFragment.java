package com.step4fun.mycanal2.canal2.views.fragment;//package com.step4fun.mycanal2.canal2.views.fragment;
//
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import com.astuetz.PagerSlidingTabStrip;
//import com.mycanal2.R;
//
///**
// * Created by stephanekamga on 18/01/15.
// */
//public class ProgramsContainerFragment extends Fragment {
//
//    private ViewPager mViewPager;
//    private ViewPagerAdapter mAdapter;
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.programs_container,container,false);
//        mViewPager = (ViewPager)view.findViewById(R.id.pager);
//        mAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
//        mViewPager.setOffscreenPageLimit(5);
//        mViewPager.setAdapter(mAdapter);
//        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
//        tabs.setViewPager(mViewPager);
//        return view;
//    }
//
//    class ViewPagerAdapter extends FragmentStatePagerAdapter {
//
//        public ViewPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int i) {
//
//            return ProgramsFragment.getInstance(i);
//        }
//
//        @Override
//        public int getCount() {
//            return 7;
//        }
//
//       }
//
//}
