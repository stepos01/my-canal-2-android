package com.step4fun.mycanal2.canal2;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.util.Log;
import android.util.Patterns;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.Period;
import retrofit.client.Response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stephanekamga on 14/01/15.
 */
public class Utils {
    public static void SaveSettings(Activity activity, String name, String value) {
        SharedPreferences preferences = activity.getPreferences(Context.MODE_PRIVATE);
        preferences.edit().putString(name, value).commit();
    }

    public static String LoadSettings(Activity activity, String name) {
        String result = null;
        SharedPreferences preferences = activity.getPreferences(Context.MODE_PRIVATE);
        if (preferences.contains(name)) {
            result = preferences.getString(name, null);
        }

        return result;
    }

    public static boolean isNullOrEmpty(String entry) {
        if (entry == null || entry.trim().length() == 0) return true;
        else return false;
    }

    public static void load(Context pContext, String mLink, ImageView view) {
        String link = mLink;
        if (Utils.isNullOrEmpty(mLink)) {
            link = resIdToUri(pContext, R.drawable.logo_big);
        }
        Glide.with(pContext).load(Uri.parse(link))
                .error(pContext.getResources().getDrawable(R.drawable.logo_big))
                .crossFade()
                .into(view);
    }

    public static boolean canUseYoutube() {
        if (YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(getActivity()).equals(YouTubeInitializationResult.SUCCESS)) {
            //This means that your device has the Youtube API Service (the app) and you are safe to launch it.
            return true;
        } else {
            Log.d("utils", "i cannot play youtube video with this device");
            return false;
        }
    }

    public static void loadVideoPicToTest(final Context pContext, String pLink, ImageView view) {
        String link = pLink;
        if (Utils.isNullOrEmpty(pLink)) {
            link = resIdToUri(pContext, R.drawable.logo_big);
        }

        Glide.with(pContext).load(Uri.parse(link))
                .asBitmap()
                .transform(new BitmapTransformation(pContext) {
                    @Override
                    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
                        Bitmap result = pool.get(outWidth, outHeight, Bitmap.Config.ARGB_8888);
                        // If no matching Bitmap is in the pool, get will return null, so we should allocate.
                        if (result == null) {
                            // Use ARGB_8888 since we're going to add alpha to the image.
                            result = Bitmap.createBitmap(outWidth, outHeight, Bitmap.Config.ARGB_8888);
                        }
                        // Create a Canvas backed by the result Bitmap.
                        Canvas canvas = new Canvas(result);
                        Bitmap bitmap = BitmapFactory.decodeResource(pContext.getResources(), R.drawable.play_blue);
                        bitmap = bitmap.copy(bitmap.getConfig(), true);
//                        Paint paint = new Paint();
                        // Draw the original Bitmap onto the result Bitmap with a transformation.
                        canvas.drawBitmap(toTransform, 0, 0, null);
                        float left = result.getWidth() / 2 - bitmap.getWidth() / 2;
                        float top = result.getHeight() / 2 - bitmap.getHeight() / 2;
                        canvas.drawBitmap(bitmap, left, top, null);
                        // Since we've replaced our original Bitmap, we return our new Bitmap here. Glide will
                        // will take care of returning our original Bitmap to the BitmapPool for us.
                        return result;
                    }

                    @Override
                    public String getId() {
                        return null;
                    }
                })
                .error(pContext.getResources().getDrawable(R.drawable.logo_big))
                .thumbnail(.1f)
                .into(view);
    }

    public static String resIdToUri(Context context, int resId) {
        return "android.resource://" + context.getPackageName()
                + "/" + +resId;
    }

    public static int getSizeName(Context context) {
        int screenLayout = context.getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenLayout) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return 1;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return 2;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return 3;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return 4;
            default:
                return 5;
        }
    }

    public static String parseStream(Response result) {
        BufferedReader reader;
        final StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        final String response = sb.toString();

        return response;
    }

    public static Activity getActivity() {
        return ((MyApplication) MyApplication.context().getApplicationContext()).getCurrentActivity();
    }

    public static String getPrimaryAccount() {
        final Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        final Account[] accounts = AccountManager.get(MyApplication.context()).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                return account.name;
            }
        }

        return null;
    }

    /**
     * This method factors the pseudo of the user by truncating
     * the email and getting the first part before "@"
     *
     * @param email
     * @return
     */
    public static String split(String email) {
        final Pattern pattern = Pattern.compile("[a-zA-Z0-9.]+");
        final Matcher matcher = pattern.matcher(email);
        matcher.find();
        final String pseudo = matcher.group(0);

        return pseudo;
    }

    public static String buildDate(long date, String format) {
        final DateTime dateTime = new DateTime(date);
        return dateTime.toString(format, getUserLocale());
    }

    public static Locale getUserLocale() {
        return MyApplication.context().getResources().getConfiguration().locale;
    }

    public static String buildUsername(String input) {
        final StringBuilder result = new StringBuilder();
        if (count(input, Pattern.quote(".")) > 0) {
            final String[] tmp = input.split(Pattern.quote("."));
            for (String str : tmp) {
                result.append(str).append(" ");
            }
        } else {
            result.append(input);
        }

        return result.toString();
    }

    public static int count(String str, String regex) {
        int i = 0;
        final Pattern p = Pattern.compile(regex);
        final Matcher m = p.matcher(str);
        while (m.find()) {
            m.group();
            i++;
        }

        return i;
    }

    public static String niceDate(long date, String format) {
        final Resources resources = MyApplication.context().getResources();
        final String empty = " ";
        final String minuteTitle = resources.getString(R.string.minute);
        final String hourTitle = resources.getString(R.string.hour);
        final String yesterdayTitle = resources.getString(R.string.yesterday);
        final String todayTitle = resources.getString(R.string.today);
        final String at = resources.getString(R.string.at);
        final String agoTitle = resources.getString(R.string.ago);
        final String fr = MyApplication.context().getResources().getString(R.string.iso_country_fr);
        final String it = MyApplication.context().getResources().getString(R.string.iso_country_it);
        final Locale locale = getUserLocale();
        final String country = locale.getCountry();
        final DateTime dateTime = new DateTime(date);
        final DateTime today = DateTime.now();
        final int days = Days.daysBetween(dateTime.toLocalDate(), today.toLocalDate()).getDays();
        final Interval interval = new Interval(dateTime, today);
        final Period period = interval.toPeriod();
        final StringBuilder res = new StringBuilder();
        final String hour = String.valueOf(dateTime.getHourOfDay());
        final String min = String.valueOf(dateTime.getMinuteOfHour());
        final String now = resources.getString(R.string.now);
        switch (days) {
            case 0:
                // User posts today
                if (country.equals(fr)) {
                    if (period.getHours() == 0) {
                        if (period.getMinutes() == 0) {
                            res.append(now);
                        } else {
                            res.append(agoTitle).append(empty).append(period.getMinutes()).append(minuteTitle);
                        }
                    } else {
                        res.append(todayTitle).append(empty).append(at)
                                .append(empty).append(dateTime.toString("HH'h'mm", locale));
                    }
                } else if (country.equals(it)) {
                    if (period.getHours() == 0) {
                        if (period.getMinutes() == 0) {
                            res.append(now);
                        } else {
                            res.append(period.getMinutes()).append(minuteTitle).append(empty).append(agoTitle);
                        }
                    } else {
                        res.append(todayTitle).append(empty).append(at)
                                .append(empty).append(dateTime.toString("HH:mm", locale));
                    }
                }else {
                    // Default representation is US or UK display
                    if (period.getHours() == 0) {
                        if (period.getMinutes() == 0) {
                            res.append(now);
                        } else {
                            res.append(period.getMinutes()).append(minuteTitle).append(empty).append(agoTitle);
                        }
                    } else {
                        res.append(todayTitle).append(empty).append(at)
                                .append(empty).append(dateTime.toString("h.mm a", locale));
                    }
                }
                break;
            case 1:
                // Post was yesterday
                if (country.equals(fr)) {
                    res.append(yesterdayTitle).append(empty).append(at).append(empty)
                            .append(hour).append(hourTitle).append(min);
                } else if (country.equals(it)) {
                    res.append(yesterdayTitle).append(empty).append(at).append(empty)
                            .append(dateTime.toString("HH:mm", locale));
                }else {
                    // Default representation is US or UK display
                    res.append(yesterdayTitle).append(empty).append(at).append(empty)
                            .append(dateTime.toString("h.mm a", locale));
                }
                break;
            default:
                // Before yesterday
                res.append(buildDate(date, format));
                break;
        }

        return res.toString();
    }
}
