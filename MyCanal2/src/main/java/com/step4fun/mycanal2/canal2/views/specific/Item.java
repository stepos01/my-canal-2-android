package com.step4fun.mycanal2.canal2.views.specific;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by sngatchou on 19/03/2015.
 */
public abstract class Item {
    public abstract View getView(LayoutInflater inflater, int position, View convertView, ViewGroup parent);

    public abstract int getItemViewType();
}

