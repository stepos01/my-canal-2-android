package com.step4fun.mycanal2.canal2.controller.networkservice;

import com.step4fun.mycanal2.canal2.model.Comments;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.QueryMap;

import java.util.HashMap;
import java.util.List;

/**
 * Created by stephanekamga on 19/03/15.
 */
public interface CommentsApi {
    @POST("/comments/")
    public void newComment(@Body Comments comments, Callback<Response> cb);

    @GET("/comments/")
    public void loadComments(@QueryMap HashMap<String, String> map, Callback<List<Comments>> vb);
}
