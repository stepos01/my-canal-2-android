package com.step4fun.mycanal2.canal2.controller.twitterbootstrap;

import com.twitter.sdk.android.core.Session;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by stephanekamga on 21/02/15.
 */
public class Canal2TwitterApiClient extends TwitterApiClient {
    public Canal2TwitterApiClient(Session session) {
        super(session);
    }

    /**
     * Provide CustomService with defined endpoints
     */
    public ProfilePictureService getProfilePictureService() {
        return getService(ProfilePictureService.class);
    }
}

// example users/show service endpoint
interface ProfilePictureService {
    @GET("/1.1/users/show.json")
    void show(@Query("user_id") long id, Callback<User> cb);
}



