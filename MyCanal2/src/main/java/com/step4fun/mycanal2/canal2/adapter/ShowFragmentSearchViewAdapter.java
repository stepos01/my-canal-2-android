package com.step4fun.mycanal2.canal2.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.model.ProgramsModel;

import java.util.List;

/**
 * Created by stephanekamga on 27/01/15.
 */
public class ShowFragmentSearchViewAdapter extends CursorAdapter {

    private List<ProgramsModel> mModelList;

    public ShowFragmentSearchViewAdapter(Context context, Cursor c, List<ProgramsModel> modelList) {
        super(context, c, false);
        mModelList = modelList;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.show_fragment_search_view_item, parent, false);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

//        String result;
//        if (mModelList.get(cursor.getPosition()).getType() == ShowsFragment.VIEW_TYPE_HEADER)
//            result = mModelList.get(cursor.getPosition()).getSection();
//        else result = mModelList.get(cursor.getPosition()).getTitle();
        TextView mTextView = (TextView) view.findViewById(R.id.searchViewItem);
        String body = cursor.getString(cursor.getColumnIndexOrThrow("text"));

        mTextView.setText(body);
    }
}
