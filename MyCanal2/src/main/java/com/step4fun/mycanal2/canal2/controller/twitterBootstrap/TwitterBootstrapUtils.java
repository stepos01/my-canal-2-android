package com.step4fun.mycanal2.canal2.controller.twitterbootstrap;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.models.User;

/**
 * Created by stephanekamga on 21/02/15.
 */
public class TwitterBootstrapUtils {
    public static boolean isUserLoggedIn()
    {
        return Twitter.getSessionManager().getActiveSession() != null;
    }
    public static void getProfilePic(Callback<User> callback)
    {
        if(isUserLoggedIn())
        {
            Canal2TwitterApiClient client = new Canal2TwitterApiClient(Twitter.getSessionManager().getActiveSession());
            long id = Twitter.getSessionManager().getActiveSession().getUserId();
            client.getProfilePictureService().show(id, callback);
        }
    }
    public static void postTweet(String content,Callback<Tweet>callback)
    {
        Twitter.getApiClient().getStatusesService().update(content,null,null,null,null,null,null,null,callback);
    }
}

