package com.step4fun.mycanal2.canal2.controller.networkservice;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;

/**
 * Created by stephanekamga on 16/03/15.
 */
public interface StreamApi {
    @GET("/stream/")
    public void getStreamUrl(Callback<Response> cb);
}
