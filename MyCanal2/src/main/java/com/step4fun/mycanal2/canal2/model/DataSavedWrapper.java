package com.step4fun.mycanal2.canal2.model;

import android.app.Activity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.step4fun.mycanal2.canal2.Utils;

/**
 * Created by stephanekamga on 31/01/15.
 */
public abstract class DataSavedWrapper<T> {


     protected String settingsId;
     private Activity context;
    public DataSavedWrapper(Activity activity, String settingsID) {
        this.settingsId = settingsID;
        this.context = activity;
    }


    protected void save(T value, TypeToken<T> token) {
        Gson parser = new Gson();

        String json = parser.toJson(value, token.getType());
        Utils.SaveSettings(context, settingsId, json);
    }

    protected T load(TypeToken<T> token)
    {
        T result;
        String json = Utils.LoadSettings(context, settingsId);
        if (!Utils.isNullOrEmpty(json)) {
            Gson parser = new Gson();

            result = parser.fromJson(json, token.getType());
        } else result = null;
        return result;
    }

}
