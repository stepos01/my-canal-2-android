package com.step4fun.mycanal2.canal2.views.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.step4fun.mycanal2.canal2.R;
import com.step4fun.mycanal2.canal2.Utils;
import com.step4fun.mycanal2.canal2.model.ShowsVideo;

/**
 * Created by stephanekamga on 28/01/15.
 */
public class ShowDetailsHeaderFragment extends Fragment implements View.OnClickListener {
    private static String MODEL_BUNDLE_TAG_LINK = "MODEL_BUNDLE_TAG_LINK";
    private static String MODEL_BUNDLE_TAG_SHOW_VIDEO = "MODEL_BUNDLE_TAG_SHOW_VIDEO";
    private static final String YOUTUBE_API_KEY = "AIzaSyAw4gpoIJzQ3SDQPx4NcbjMGvhxSwbo5eg";
    private String TAG = getClass().getSimpleName();
    private ShowsVideo showsVideo;


    public static ShowDetailsHeaderFragment getFragmentInstance(String link, ShowsVideo showsVideo) {
        ShowDetailsHeaderFragment fragment = new ShowDetailsHeaderFragment();
        Bundle b = new Bundle();
        b.putString(MODEL_BUNDLE_TAG_LINK, link);
        b.putParcelable(MODEL_BUNDLE_TAG_SHOW_VIDEO, showsVideo);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String mLink = getArguments().getString(MODEL_BUNDLE_TAG_LINK);
        showsVideo = getArguments().getParcelable(MODEL_BUNDLE_TAG_SHOW_VIDEO);
        RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.shows_details_head, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.shows_details_head_image);
        if (showsVideo == null) {
            //it's an image
            view.removeView(view.findViewById(R.id.shows_details_head_image_overlay));
            view.removeView(view.findViewById(R.id.shows_details_head_image_playButton));
            Utils.load(getActivity(), mLink, imageView);
        } else {
            view.findViewById(R.id.shows_details_head_image_playButton).setOnClickListener(this);
            Utils.load(getActivity(), showsVideo.getImageUrl(), imageView);
        }

        return view;
    }

    private boolean canUseYoutube() {
        if (YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(getActivity()).equals(YouTubeInitializationResult.SUCCESS)) {
            //This means that your device has the Youtube API Service (the app) and you are safe to launch it.
            return true;
        } else {
            Log.d(TAG, "i cannot play youtube video with this device");
            return false;
        }

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if (Utils.canUseYoutube()) {
            if (!Utils.isNullOrEmpty(showsVideo.getVideoId()) && showsVideo.isPlaylist) {
                intent = YouTubeStandalonePlayer.createPlaylistIntent(getActivity(), YOUTUBE_API_KEY,
                        showsVideo.getVideoId(), 0, 0, true, false);
            } else if (!Utils.isNullOrEmpty(showsVideo.getVideoId()) && !showsVideo.isPlaylist) {
                intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(),
                        YOUTUBE_API_KEY, showsVideo.getVideoId(), 0, true, false);
            }
            if (intent != null) startActivity(intent);
        } else {
            if (!Utils.isNullOrEmpty(showsVideo.getVideoId()) && !showsVideo.isPlaylist) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + showsVideo.videoId)));
            } else if (!Utils.isNullOrEmpty(showsVideo.getVideoId()) && showsVideo.isPlaylist) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/playlist?list=" + showsVideo.videoId)));
            }
        }
    }
}
