//package com.step4fun.mycanal2.canal2.model;
//
//import android.app.Activity;
//import org.joda.time.LocalDateTime;
//
///**
// * Created by stephanekamga on 31/01/15.
// */
//public class StreamUrlData extends DataSavedWrapper {
//    private static final String SETTINGS_ID = "StreamUrlData_SETTINGS_ID";
//    private Data<String> result;
//    public StreamUrlData(Activity activity) {
//        super(activity, SETTINGS_ID);
//    }
//
//    @Override
//    public boolean isDataOld() {
//       if(result == null)return true;
//        LocalDateTime dateTime = new LocalDateTime(result.date);
//        return dateTime.plusHours(2).isBefore(LocalDateTime.now());
//
//
//    }
//
//    public String loadUrl()
//    {
//        result = super.load();
//        if(isDataOld())return null;
//        else return result.value;
//    }
//    public void saveUrl(String url)
//    {
//        super.save(url);
//    }
//}
