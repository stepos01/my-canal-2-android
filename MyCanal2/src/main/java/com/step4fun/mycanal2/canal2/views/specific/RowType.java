package com.step4fun.mycanal2.canal2.views.specific;

/**
 * Created by sngatchou on 19/03/2015.
 */
public enum RowType {
    PUB("advertising"),
    HEADER("header"),
    ROW("row"),
    NO_COMMENTS("no_comments"),
    ERROR("error");

    private String type;

    RowType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
