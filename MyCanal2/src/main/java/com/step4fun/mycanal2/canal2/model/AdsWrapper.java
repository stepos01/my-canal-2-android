package com.step4fun.mycanal2.canal2.model;

import android.content.Context;
import android.view.View;
import com.google.android.gms.ads.AdView;
import com.step4fun.mycanal2.canal2.R;

/**
* Created by stephanekamga on 24/01/15.
*/
public class AdsWrapper {

    public AdView mAdView;
    public Context mContext;
    public AdsWrapper(Context context,View baseView)
    {
        mAdView = (AdView)baseView.findViewById(R.id.adlayoutview);
    }

    public AdView getAdView() {
        return mAdView;
    }
}
