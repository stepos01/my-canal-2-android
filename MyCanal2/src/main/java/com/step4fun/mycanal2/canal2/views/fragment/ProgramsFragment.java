package com.step4fun.mycanal2.canal2.views.fragment;//package com.step4fun.mycanal2.canal2.views.fragment;
//
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ListView;
//import com.mycanal2.R;
//import com.mycanal2.controller.MockInfoProvider;
//import com.mycanal2.controller.ProgramsInfoProvider;
//import com.mycanal2.controller.adapter.ProgramListViewAdapter;
//import com.mycanal2.model.ProgramsModel;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by stephanekamga on 17/01/15.
// */
//public class ProgramsFragment extends Fragment implements ProgramsInfoProvider.LoadInfoCompletedListener, AdapterView.OnItemClickListener {
//
//    public static final String DAYNUMBERTAG = "DAYNUMBERTAG";
//    private static final String DIALOGTAG = "DIALOGTAG";
//    private ProgramsInfoProvider mInfoProvider;
//    private ArrayList<ProgramsModel> mProgramList;
//    private ProgramListViewAdapter mAdapter;
//    private ListView mListView;
//
//    public static ProgramsFragment getInstance(int dayNumber)
//    {
//        ProgramsFragment programsFragmentInstance = new ProgramsFragment();
//        Bundle b = new Bundle();
//        b.putInt(DAYNUMBERTAG,dayNumber);
//        programsFragmentInstance.setArguments(b);
//        return programsFragmentInstance;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.programs,container,false);
//        mListView = (ListView)view.findViewById(R.id.programs_listView);
//        mInfoProvider =  MockInfoProvider.getInstance();
//        mInfoProvider.setLoadInfoCompleted(this);
//        mInfoProvider.loadInfo();
//        mListView.setOnItemClickListener(this);
//        return view;
//
//
//
//    }
//
//    @Override
//    public void loadInfoCompleted(String title,List<ProgramsModel> list) {
//        mProgramList = new ArrayList<ProgramsModel>(list);
//        mAdapter = new ProgramListViewAdapter(getActivity(), R.layout.programs_item,mProgramList);
//        mListView.setAdapter(mAdapter);
//
//    }
//
//    @Override
//    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//        ProgramsDetailDialog dialog = ProgramsDetailDialog.getInstance(mAdapter.getItem(i));
//        dialog.show(getActivity().getSupportFragmentManager(),DIALOGTAG);
//    }
//}
