package com.cybozu.labs.langdetect;

import java.util.ArrayList;

/**
 * {@link com.cybozu.labs.langdetect.Language} is to store the detected language.
 * {@link com.cybozu.labs.langdetect.Detector#getProbabilities()} returns an {@link java.util.ArrayList} of {@link com.cybozu.labs.langdetect.Language}s.
 *  
 * @see com.cybozu.labs.langdetect.Detector#getProbabilities()
 * @author Nakatani Shuyo
 *
 */
public class Language {
    public String lang;
    public double prob;
    public Language(String lang, double prob) {
        this.lang = lang;
        this.prob = prob;
    }
    public String toString() {
        if (lang==null) return "";
        return lang + ":" + prob;
    }
}
